<?php
/**
 * The template for displaying the onepage header
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
    <?php electron_favico_icon(); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php
    $preloader_display = get_post_meta( get_the_ID(), 'onepage_preloader_display', true );
    $preloader = get_post_meta( get_the_ID(), 'onepage_preloader', true );
    if($preloader == '') $preloader = ELECTRONURI.'/img/Preloader.gif';
    if( $preloader_display != 'off' ):
    ?>
	<div id="preloader">
        <img src="<?php echo esc_url($preloader); ?>" alt="" />
    </div>
    <?php endif; 
        $nav_display = get_post_meta( get_the_ID(), 'nav_display', true );
        $nav_style =  get_post_meta( get_the_ID(), 'nav_style', true );
        $nav_style = ($nav_style != '')? $nav_style : 'standard';
        $nav_style = ( $nav_display == 'off' )? 'off' : $nav_style;
        get_template_part('header/onepage-nav', $nav_style);        
    ?>
    <div id="home">
        <?php 
        if ( have_posts() ) : 
        // Start the loop.
            while ( have_posts() ) : the_post();
                the_content();
            endwhile;
        endif;
        ?>            
    </div><!-- #home -->