<form role="search" method="get" id="searchform" class="bp-searchform searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="form-group">
		<input type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php echo __( 'Type and hit enter...', 'electron' ); ?>" />
		<input type="submit" hidden="hidden">
	</div>
</form>