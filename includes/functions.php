<?php
if ( ! function_exists( 'electron_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Fifteen.
 *
 * @return string Google fonts URL for the theme.
 */
function electron_fonts_url() {
	$fonts_url = '';
	$fonts     = array();

	/*
	 * Translators: If there are characters in your language that are not supported
	 */
	$fonts = array('Raleway:400,100,200,300,500,600,700,800,900', 'Oswald:400,300,700&subset=latin,latin-ext');
	

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
		), '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;
// Register Style
function electron_styles() {

	wp_register_style( 'electron-bootstrap', ELECTRONURI . '/css/bootstrap.min.css', false, false );
	wp_enqueue_style( 'electron-bootstrap' );

	wp_register_style( 'electron-fonts', electron_fonts_url(), false, false );
	wp_enqueue_style( 'electron-fonts' );

	wp_register_style( 'electron-font-awesome', ELECTRONURI . '/fonts/font-awesome/css/font-awesome.min.css', false, false );
	wp_enqueue_style( 'electron-font-awesome' );

	wp_register_style( 'electron-color', ELECTRONURI . '/css/color.css', false, ELECTRONVERSION );
	wp_enqueue_style( 'electron-color' );

	wp_register_style( 'electron-typography', ELECTRONURI . '/css/typography.css', false, ELECTRONVERSION );
	wp_enqueue_style( 'electron-typography' );

	wp_dequeue_style( 'ot-dynamic-font_css' );
	wp_register_style( 'electron-dynamic-css', get_stylesheet_directory_uri() . '/dynamic.css', false, ELECTRONVERSION );
	wp_enqueue_style( 'electron-dynamic-css' );

	wp_register_style( 'electron-main-style', ELECTRONURI . '/css/style.css', false, ELECTRONVERSION );
	wp_enqueue_style( 'electron-main-style' );

	wp_enqueue_style( 'electron-style', get_stylesheet_uri() );
	
}
add_action( 'wp_enqueue_scripts', 'electron_styles' );

// Register Script
function electron_scripts() {

	$api = ot_get_option( 'google_map_api', '' );
	$api = ( $api != '' )? '&key='.esc_attr($api) : '';
	wp_register_script( 'electron-maps-api', '//maps.google.com/maps/api/js?sensor=false'.$api, array( 'jquery' ), false, false );

	wp_register_script( 'electron-directions', ELECTRONURI . '/js/directions.js', array( 'electron-maps-api' ), false, false );	

	wp_register_script( 'electron-modernizr', ELECTRONURI . '/js/modernizr.custom.js', false, false, false );
	wp_enqueue_script( 'electron-modernizr' );	

	wp_register_script( 'electron-bootstrap', ELECTRONURI . '/js/bootstrap.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'electron-bootstrap' );

	wp_register_script( 'electron-placeholders', ELECTRONURI . '/js/placeholders.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'electron-placeholders' );

	wp_register_script( 'electron-superslides', ELECTRONURI . '/js/jquery.superslides.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'electron-superslides' );

	wp_register_script( 'electron-countdown', ELECTRONURI . '/js/jquery.countdown.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'electron-countdown' );

	wp_register_script( 'electron-carousel', ELECTRONURI . '/js/owl.carousel.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'electron-carousel' );

	wp_register_script( 'electron-custom', ELECTRONURI . '/js/custom.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'electron-custom' );

	wp_register_script( 'electron-master', ELECTRONURI . '/js/master.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'electron-master' );

	$arr = array( 
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'ELECTRONURI' => ELECTRONURI,
		'ELECTRONDIR' => ELECTRONDIR,
		'CountdownText' => apply_filters( 'electron_CountdownText', array('years', 'days', 'hrs', 'min', 'sec'))
		);
	wp_localize_script( 'electron-master', 'electron', $arr );

	// Load the html5 shiv.
	wp_enqueue_script( 'electron-html5', ELECTRONURI . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'electron-html5', 'conditional', 'lt IE 9' );

}
add_action( 'wp_enqueue_scripts', 'electron_scripts' );

function electron_post_categories(){
	global $post;
	$categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'electron' ) );
		if ( $categories_list ) {
			printf( '%1$s',
				$categories_list
			);
		}
}
// Content excerpt length
function electron_excerpt_length( $length ) {
	$excerpt_length = ot_get_option('excerpt_length', 55);
	return esc_attr(get_theme_mod( 'excerpt_length', $excerpt_length ));
}
add_filter( 'excerpt_length', 'electron_excerpt_length', 999 );

function electron_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'electron_excerpt_more');

function electron_post_nav(){
	$navigation = '';
	$navigation = '';
 
    $previous = get_previous_post_link('<div class="post-previous">&laquo; %link</div>');
 
    $next = get_next_post_link( '<div class="post-next">%link &raquo;</div>');
 
    // Only add markup if there's somewhere to navigate to.
    if ( $previous || $next ) {
        $navigation = _navigation_markup( $previous . $next, 'post-navigation', '' );
    }
 
	echo force_balance_tags($navigation);
}



if(!( function_exists('electron_pagination') )){
	function electron_pagination($pages = '', $range = 2){
		$showitems = ($range * 2)+1;
		
		global $paged, $wp_query;
		if(empty($paged)) $paged = 1;
		
		if($pages == ''){
			global $wp_query;
			$pages = $wp_query->max_num_pages;
				if(!$pages) {
					$pages = 1;
				}
		}
		
		$output = '';
			
		if(1 != $pages){
			$output .= "<div class='pagination-wrapper'><ul class='pagination'>";
			if($paged > 2 && $paged > $range+1 && $showitems < $pages) $output .= "<li><a href='".get_pagenum_link(1)."' aria-label='Previous'>&laquo;</a></li> ";
			
			for ($i=1; $i <= $pages; $i++){
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
					$output .= ($paged == $i)? "<li class='active'><a href='".get_pagenum_link($i)."'>".$i."</a></li> ":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li> ";
				}
			}
		
			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) $output .= "<li><a href='".get_pagenum_link($pages)."' aria-label='Next'>&raquo;</a></li> ";
			$output.= "</ul></div>";
		}
		
		return $output;
	}
}

if ( ! function_exists( 'electron_comment_nav' ) ) :
/**
 * Display navigation to next/previous comments when applicable.
 *
 * @since Twenty Fifteen 1.0
 */
function electron_comment_nav() {
	// Are there comments to navigate through?
	if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
	?>
	<nav class="navigation comment-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Comment navigation', 'electron' ); ?></h2>
		<div class="nav-links">
			<?php
				if ( $prev_link = get_previous_comments_link( __( 'Older Comments', 'electron' ) ) ) :
					printf( '<div class="nav-previous">%s</div>', $prev_link );
				endif;

				if ( $next_link = get_next_comments_link( __( 'Newer Comments', 'electron' ) ) ) :
					printf( '<div class="nav-next">%s</div>', $next_link );
				endif;
			?>
		</div><!-- .nav-links -->
	</nav><!-- .comment-navigation -->
	<?php
	endif;
}
endif;

function electron_comment_form( $args = array(), $post_id = null ) {
	if ( null === $post_id )
		$post_id = get_the_ID();

	$commenter = wp_get_current_commenter();
	$user = wp_get_current_user();
	$user_identity = $user->exists() ? $user->display_name : '';

	$args = wp_parse_args( $args );
	if ( ! isset( $args['format'] ) )
		$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

	$req      = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$html_req = ( $req ? " required='required'" : '' );
	$html5    = 'html5' === $args['format'];
	$fields   =  array(
		'author' => '<div class="form-row col-sm-4">' .
		            '<input id="author" placeholder="'.__('Name', 'electron').'" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . $html_req . ' /></div>',
		'email'  => '<div class="form-row col-sm-4">' .
		            '<input id="email" placeholder="'.__('Email', 'electron').'" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></div>',
		'url'    => '<div class="form-row col-sm-4">' .
		            '<input id="url" placeholder="'.__('Subject', 'electron').'" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" /></div>',
	);

	$required_text = sprintf( ' ' . __('Required fields are marked %s', 'electron'), '<span class="required">*</span>' );

	/**
	 * Filter the default comment form fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $fields The default comment fields.
	 */
	$fields = apply_filters( 'comment_form_default_fields', $fields );
	$defaults = array(
		'fields'               => $fields,
		'comment_field'        => '<div class="form-row col-sm-12"><textarea placeholder="'.__('Write your comment here...', 'electron').'" id="comment" name="comment"  rows="3" cols="60"  aria-required="true" required="required"></textarea></div>',
		/** This filter is documented in wp-includes/link-template.php */
		'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'electron' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
		/** This filter is documented in wp-includes/link-template.php */
		'logged_in_as'         => '<p class="logged-in-as">' . sprintf( __( '<a href="%1$s" aria-label="Logged in as %2$s. Edit your profile.">Logged in as %2$s</a>. <a href="%3$s">Log out?</a>', 'electron' ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
		'comment_notes_before' => '',
		'comment_notes_after'  => '',
		'id_form'              => 'commentform',
		'id_submit'            => 'submit',
		'class_form'           => 'comment-form',
		'class_submit'         => 'submit',
		'name_submit'          => 'submit',
		'title_reply'          => __( 'Leave a Reply', 'electron' ),
		'title_reply_to'       => __( 'Leave a Reply to %s', 'electron' ),
		'title_reply_before'   => '<h3 id="reply-title" class="comment-reply-title comment-reply-title">',
		'title_reply_after'    => '</h3>',
		'cancel_reply_before'  => ' <small>',
		'cancel_reply_after'   => '</small>',
		'cancel_reply_link'    => __( 'Cancel reply', 'electron' ),
		'label_submit'         => __( 'Post Comment', 'electron' ),
		'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="%3$s btn btn-dark btn-lg" value="&#xf1d8; %4$s" />',
		'submit_field'         => '<div class="form-row text-center">%1$s %2$s</div>',
		'format'               => 'xhtml',
	);

	/**
	 * Filter the comment form default arguments.
	 *
	 * Use 'comment_form_default_fields' to filter the comment fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $defaults The default comment form arguments.
	 */
	$args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

	// Ensure that the filtered args contain all required default values.
	$args = array_merge( $defaults, $args );

	if ( comments_open( $post_id ) ) : ?>
		<div class="row">
        <div class="col-md-12 col-sm-12">
		<?php
		/**
		 * Fires before the comment form.
		 *
		 * @since 3.0.0
		 */
		do_action( 'comment_form_before' );
		?>
		<div id="respond" class="comment-respond">
			<?php
			echo $args['title_reply_before'];

			comment_form_title( $args['title_reply'], $args['title_reply_to'] );

			echo $args['cancel_reply_before'];

			cancel_comment_reply_link( $args['cancel_reply_link'] );

			echo $args['cancel_reply_after'];

			echo $args['title_reply_after'];

			if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) :
				echo $args['must_log_in'];
				/**
				 * Fires after the HTML-formatted 'must log in after' message in the comment form.
				 *
				 * @since 3.0.0
				 */
				do_action( 'comment_form_must_log_in_after' );
			else : ?>
				
				<div id="contact_form"><form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="<?php echo esc_attr( $args['class_form'] ); ?>"<?php echo esc_attr($html5) ? ' novalidate' : ''; ?>>
					<?php
					/**
					 * Fires at the top of the comment form, inside the form tag.
					 *
					 * @since 3.0.0
					 */
					do_action( 'comment_form_top' );

					if ( is_user_logged_in() ) :
						/**
						 * Filter the 'logged in' message for the comment form for display.
						 *
						 * @since 3.0.0
						 *
						 * @param string $args_logged_in The logged-in-as HTML-formatted message.
						 * @param array  $commenter      An array containing the comment author's
						 *                               username, email, and URL.
						 * @param string $user_identity  If the commenter is a registered user,
						 *                               the display name, blank otherwise.
						 */
						echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );

						/**
						 * Fires after the is_user_logged_in() check in the comment form.
						 *
						 * @since 3.0.0
						 *
						 * @param array  $commenter     An array containing the comment author's
						 *                              username, email, and URL.
						 * @param string $user_identity If the commenter is a registered user,
						 *                              the display name, blank otherwise.
						 */
						do_action( 'comment_form_logged_in_after', $commenter, $user_identity );

					else :

						echo $args['comment_notes_before'];

					endif;

					// Prepare an array of all fields, including the textarea
					$comment_fields = (array) $args['fields'] + array( 'comment' => $args['comment_field'] );

					/**
					 * Filter the comment form fields.
					 *
					 * @since 4.4.0
					 *
					 * @param array $comment_fields The comment fields.
					 */
					$comment_fields = apply_filters( 'comment_form_fields', $comment_fields );

					// Get an array of field names, excluding the textarea
					$comment_field_keys = array_diff( array_keys( $comment_fields ), array( 'comment' ) );

					// Get the first and the last field name, excluding the textarea
					$first_field = reset( $comment_field_keys );
					$last_field  = end( $comment_field_keys );
					echo '<div class="row">';
					foreach ( $comment_fields as $name => $field ) {

						if ( 'comment' === $name ) {

							/**
							 * Filter the content of the comment textarea field for display.
							 *
							 * @since 3.0.0
							 *
							 * @param string $args_comment_field The content of the comment textarea field.
							 */
							echo apply_filters( 'comment_form_field_comment', $field );

							echo $args['comment_notes_after'];

						} elseif ( ! is_user_logged_in() ) {
							

							if ( $first_field === $name ) {
								/**
								 * Fires before the comment fields in the comment form, excluding the textarea.
								 *
								 * @since 3.0.0
								 */
								do_action( 'comment_form_before_fields' );
							}

							/**
							 * Filter a comment form field for display.
							 *
							 * The dynamic portion of the filter hook, `$name`, refers to the name
							 * of the comment form field. Such as 'author', 'email', or 'url'.
							 *
							 * @since 3.0.0
							 *
							 * @param string $field The HTML-formatted output of the comment form field.
							 */
							echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";

							if ( $last_field === $name ) {
								/**
								 * Fires after the comment fields in the comment form, excluding the textarea.
								 *
								 * @since 3.0.0
								 */
								do_action( 'comment_form_after_fields' );
							}

						}
					}
					echo '</div>';

					$submit_button = sprintf(
						$args['submit_button'],
						esc_attr( $args['name_submit'] ),
						esc_attr( $args['id_submit'] ),
						esc_attr( $args['class_submit'] ),
						esc_attr( $args['label_submit'] )
					);

					/**
					 * Filter the submit button for the comment form to display.
					 *
					 * @since 4.2.0
					 *
					 * @param string $submit_button HTML markup for the submit button.
					 * @param array  $args          Arguments passed to `comment_form()`.
					 */
					$submit_button = apply_filters( 'comment_form_submit_button', $submit_button, $args );

					$submit_field = sprintf(
						$args['submit_field'],
						$submit_button,
						get_comment_id_fields( $post_id )
					);

					/**
					 * Filter the submit field for the comment form to display.
					 *
					 * The submit field includes the submit button, hidden fields for the
					 * comment form, and any wrapper markup.
					 *
					 * @since 4.2.0
					 *
					 * @param string $submit_field HTML markup for the submit field.
					 * @param array  $args         Arguments passed to comment_form().
					 */
					echo apply_filters( 'comment_form_submit_field', $submit_field, $args );

					/**
					 * Fires at the bottom of the comment form, inside the closing </form> tag.
					 *
					 * @since 1.5.0
					 *
					 * @param int $post_id The post ID.
					 */
					do_action( 'comment_form', $post_id );
					?>
				</form></div>
			<?php endif; ?>
		</div><!-- #respond -->
		</div></div>
		<?php
		/**
		 * Fires after the comment form.
		 *
		 * @since 3.0.0
		 */
		do_action( 'comment_form_after' );
	else :
		/**
		 * Fires after the comment form if comments are closed.
		 *
		 * @since 3.0.0
		 */
		do_action( 'comment_form_comments_closed' );
	endif;
}

if(!( function_exists('electron_breadcrumbs') )){ 
	function electron_breadcrumbs() {
		if ( is_front_page() || is_search() || is_404() ) {
			return;
		}
		global $post;
		$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
		$before = '<ol class="breadcrumb breadcrumb-menubar"><li>';
		$after = '</li></ol>';
		$home_text = ot_get_option('bredcrumb_menu_prefix', 'Home');
		$home = '<a href="' . esc_url( home_url( "/" ) ) . '" rel="home">' . $home_text . '</a> / ';
		
		if( 'performer' == get_post_type() ){
			$home .= '<a href="' . esc_url( home_url( "/performer/" ) ) . '">' . __( 'Performer', 'electron' ) . '</a> / ';
		}
		
		if( 'facility' == get_post_type() ){
			$home .= '<a href="' . esc_url( home_url( "/facility/" ) ) . '">' . __( 'Facilities', 'electron' ) . '</a> / ';
		}
		
		$breadcrumb = '';
		if ( $ancestors ) {
			foreach ( $ancestors as $ancestor ) {
				$breadcrumb .= '<a href="' . esc_url( get_permalink( $ancestor ) ) . '">' . esc_html( get_the_title( $ancestor ) ) . '</a> / ';
			}
		}
		
		if( is_home() ){
			$breadcrumb .= esc_html( get_option('blog_title','Our Blog') );
		} elseif( is_post_type_archive('product') ){
			//nothing
		} else {
			$breadcrumb .= esc_html( get_the_title( $post->ID ) );
		}
		
		return $before . $home . $breadcrumb . $after;
	}
}

//electron_comments
if( !function_exists('electron_comments') ):
	function electron_comments($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		extract($args, EXTR_SKIP);

		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
		?>
		<<?php echo esc_attr($tag) ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		
		<article id="div-comment-<?php comment_ID() ?>" class="comment-body">

			<footer class="comment-meta">
				<div class="comment-author vcard">
					<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
					<?php printf( __( '<b class="author-title fn">%s</b>', 'electron' ), get_comment_author_link() ); ?>					
				</div>	
				<div class="comment-meta commentmetadata comment-metadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
					<?php
						/* translators: 1: date, 2: time */
						printf( __('%1$s at %2$s', 'electron'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'electron' ), '  ', '' );
					?>
				</div>
			
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<br /><em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'electron' ); ?></em>						
				<?php endif; ?>			
			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>	
			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div>	
		</article>

	<?php
	}
endif;
/* Layout option for electron */
function electron_layout_option_values( $options = array() ){
	global $wp_query;
	
	if( is_page() ):		
		$layout = get_post_meta( get_the_ID(), 'page_layout', true );
		if(!$layout) $layout = 'rs';
		$sidebar = 	get_post_meta( get_the_ID(), 'sidebar', true );	
		$sidebar = ( $sidebar== '' )? 'sidebar-1' : $sidebar;
	elseif( is_single() ):
		$layout = ot_get_option('single_layout', 'rs');	
		$sidebar = 	ot_get_option( 'blog_single_sidebar', 'sidebar-1' );	
	else:
		$layout = ot_get_option('blog_layout', 'rs');
		$sidebar = 	ot_get_option( 'blog_sidebar', 'sidebar-1' );
	endif;

	if( is_singular('performer') ){
		$layout = 'full';	
	}

	$layout = ( $layout == '' )	? 'full' : $layout;

	$options['layout'] = $layout;
	$options['sidebar'] = ( $layout != 'full' )? $sidebar : '';

	return apply_filters(  'electron_layout_option_values', $options );
	
}

function electron_get_layout(){
	global $wp_query;
    return $wp_query->electron['layout'];
}

function electron_get_sidebar_id(){
	global $wp_query;
    return $wp_query->electron['sidebar'];
}

//add layout option
function electron_body_class($classes ){
	global $wp_query;
	$wp_query->electron = electron_layout_option_values();
	if ( is_page_template( 'templates/one-page.php' ) ) {		
		$classes[] = 'one-page-layout';		
	}else{
		$classes[] = 'multi-page-layout';
	}


	return $classes;
}
add_filter( 'body_class', 'electron_body_class' );

function electron_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    ' . __( "To view this protected post, enter the password below:", "electron" ) . '
    <div class="row"><div class="col-md-6"><input class="post-password-input form-control" name="post_password" placeholder="' . __( "Password:", "electron" ) . '" id="' . $label . '" type="password" size="20" maxlength="20" /></div><div class="col-md-6"><input type="submit" class="submit btn btn-dark btn-lg" name="Submit" value="' . esc_attr__( "Submit", "electron" ) . '" /></div></div>
    </form>
    ';
    return $o;
}
add_filter( 'the_password_form', 'electron_password_form' );

include( ELECTRONDIR.'/includes/styles.php' );