<?php
function electron_hex2rgb( $color, $opacity='1' ) {
  $color = trim( $color, '#' );

  if ( strlen( $color ) == 3 ) {
    $r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
    $g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
    $b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
  } else if ( strlen( $color ) == 6 ) {
    $r = hexdec( substr( $color, 0, 2 ) );
    $g = hexdec( substr( $color, 2, 2 ) );
    $b = hexdec( substr( $color, 4, 2 ) );
  } else {
    return '';
  }

  return "rgba( {$r}, {$g}, {$b}, {$opacity} )";
}
/**
 * Returns CSS for the color schemes.
 *
 * @param array $colors Color scheme colors.
 * @return string Color scheme CSS.
 */
function electron_get_color_scheme_css() {
	
	/*color options*/
	$preset_color_1 = ot_get_option('preset_color_1', '#f88988');
	$preset_color_2 = ot_get_option('preset_color_2', '#ff9000');
	$gray_color = ot_get_option( 'gray_color', '#f8f8f8' );
	$dark_color = ot_get_option( 'dark_color', '#273034' );
	$dark_acent_color = ot_get_option( 'dark_acent_color', '#232323' );

	$css = '
.post-wrap ul li:before, .tab-content ul li:before,
.price span, h1 span, i.prime, ul.list-default li:before, .offer h1, .tab-content h3 i, h1 span, h2 span, #upcoming .item .info i, 
.standard-menu.visible .navbar-default .navbar-nav > li > a i, .standard-menu.visible .navbar-default .navbar-nav > li > a span, .standard-menu.visible .navbar-nav li .sub-nav li a i {
    color: '.$preset_color_1.';
} 
p.entry-footer a,
.prime .dblBorder:hover, .prime .dblBorder:hover .dblBorderChild {
    border-color: '.$preset_color_1.';
}
#slides h3, .btn-primary, .BGprime, h3 span, .option li:hover, .btn-primary, .btn-dark:hover, a.top, .sqaureIconPrime, .owl-theme .owl-controls .owl-buttons div, #upcoming .item:hover, .standard-menu.BGlight .navbar-default .navbar-nav > li:hover, .navbar-nav li .sub-nav li:hover > a, .eventData .btn:hover, #contact_form input[type="submit"]:hover, .subscribe input[type=submit] {
    background-color: '.$preset_color_1.';
} 
/* ======================= */
/* ======= SECONDARY ======= */
/* ======================= */
#celebs .item i, .main-menu li:hover > a i, .main-menu li:hover > a span, #venue strong, #venue label {
    color: '.$preset_color_2.';
}
.secondary .dblBorder:hover, .secondary .dblBorder:hover .dblBorderChild {
    border-color: '.$preset_color_2.';
}
#slides .slide .slide-caption .container span, .pricing .package .inner, .pricing hr, #testimonial-slider span, #subscribeForm input[type="text"] {
    border-color: '.$preset_color_2.';
}
/* == Background SECONDARY == */
.navbar-default, .BGsecondary, .option li, .btn-default, #testimonial-slider .carousel-indicators li.active, .galleryImg > a span, .sqaureIconSec, .owl-theme .owl-controls .owl-page span, .tabs-left li.active, .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus, .nav-tabs > li > a:hover, .nav-tabs > li > a:focus, .pricing .package:hover, #upcoming .item .img, #testimonial-slider .carousel-indicators li, #slides, #venue .btn:hover, .featureBox:hover .sqaureIconPrime {
    background-color: '.$preset_color_2.';
}
/* == Background LIGHT == */
.BGlight, .services, #upcoming .item, .standard-menu.BGlight.visible {
    background-color: '.$gray_color.';
}
/* ======================= */
/* =====  DARK GREY  ===== */
/* ======================= */
h1, h2, h3, h4, h5, h6, h6 a, .phone small, .offer h1 span, .standard-menu.BGlight.visible .navbar-default .navbar-nav > li > a, .features p {
    color: '.$dark_color.';
}
/* == Background Color4 == */
#slides .slide .slide-caption .container h3 span, .BGdark, .phone:hover .BGsecondary, .btn:hover, .middleBox, .btn-dark, .galleryImg, .social ul li a i, ul.tabs-left, #venue h2, .experts .highlightBox, #slides .slide .img {
    background-color: '.$dark_color.';
}
nav.nav, .standard-menu.BGdark, .eventData .dblBorder:hover .dblBorderChild, .off-canvas-menu.BGdark {
    background-color: '.$dark_acent_color.' !important;
}	
#subscribeForm input[type="email"] {
	border-color: '.electron_hex2rgb($preset_color_1, .8).';
}
/* == use rgba values here == */
.contactForm .form-control:focus, .sqaureIconPrime, .subscribe input[type=submit] {
	-webkit-box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_1, .5).';
	-moz-box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_1, .5).';
	box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_1, .5).';
}
.BGprime.opaque {
	background-color: '.electron_hex2rgb($preset_color_1, .8).';
}

/* == use rgba values here == */
.BGsblue.opaqueBG, .member .info {
	background-color: '.electron_hex2rgb($preset_color_2, .8).';
}
.highlight-overlay.light-overlay:after,
.features .boxBg {
	background-color: '.electron_hex2rgb($preset_color_2, .3).';
}
.sqaureIconSec, .featureBox:hover .sqaureIconPrime {
	-webkit-box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_2, .5).';
	-moz-box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_2, .5).';
	box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_2, .5).';
}
@media screen and (max-width: 767px) {
	.standard-menu .navbar{ background-color: '.$preset_color_2.'; }
}
';


	return $css;
}

/**
 * Output an Underscore template for generating CSS for the color scheme.
 *
 * The template generates the css dynamically for instant display in the Customizer
 * preview.
 *
 */
function electron_color_scheme_css() {	
	wp_add_inline_style( 'electron-color', electron_get_color_scheme_css() );
}
add_action( 'wp_enqueue_scripts', 'electron_color_scheme_css' );
