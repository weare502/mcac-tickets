<?php
/**
 * The template for displaying the header
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
    <?php electron_favico_icon(); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php if( ot_get_option('show_preloader', 'on') == 'on' ): ?>
		<div id="preloader">
            <img src="<?php echo esc_url(ot_get_option('preloader', ELECTRONURI.'/img/Preloader.gif')); ?>" alt="" />
        </div>
    <?php endif; ?>

        <?php  get_template_part('header/nav', ot_get_option( 'nav_style', 'standard')); ?>

        <?php get_template_part( 'header/header', (is_page())? 'page' : get_post_type() ); ?>      