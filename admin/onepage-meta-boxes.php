<?php
function electron_get_page_sections_info(){
    global $wpdb;
    $output = '';
    if(!isset($_GET['post'])) return $output;
    if(null === $_GET['post']) return $output;
    $section = get_post_meta($_GET['post'], 'pages', true);
    if(empty($section)) return $output;

    $output .= '<ul class="option-tree-setting-wrap electron-section-content-edit">';
    foreach ($section as $key => $value) {
        if($value['page_id'] != '')
            $output .= '<li class="list-list-item"><div class="option-tree-setting"><a target="_blank" href="'.get_edit_post_link( $value['page_id'] ).'"><span class="icon ot-icon-pencil"></span> Edit page</a><span class="slug">#'.electron_get_slug($value['page_id']).'</span></div></li>';
        else
            $output .= '<li class="list-list-item"><div class="option-tree-setting"><span style="color:#a00">Section page is not selected !!!</span></div></li>';
    }
    $output .= '</ul>';

    return $output;
}
/**
 * Initialize the meta boxes. 
 */
add_action('admin_init', 'electron_onepage_meta_boxes');
function electron_onepage_meta_boxes() {
    global $wpdb;
    if (function_exists('ot_get_option')):
        $my_meta_box = array(
            'id' => 'electron_onepage_meta_box',
            'title' => 'Electron One-page Template Settings',
            'desc' => 'This option only applicable when you select one page Template. Selectd page will appear as a section in onepage.',
            'pages' => array(
                'page'
            ),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'id' => 'onepage_general_settings',
                    'label' => 'General settings',
                    'type' => 'tab',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'onepage_preloader_display',
                    'label' => __('Preloader display', 'electron'),
                    'desc' => '',
                    'std' => 'on',
                    'type' => 'on-off',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'onepage_preloader',
                    'label' => __('Preloader', 'electron'),
                    'desc' => '',
                    'std' => ELECTRONURI . '/img/Preloader.gif',
                    'type' => 'upload',    
                    'condition' => 'onepage_preloader_display:is(on)',
                    'operator' => 'and',
                    'choices' => array()
                ),
                array(
                    'id' => 'onepage_logo',
                    'label' => __('Onepage logo', 'electron'),
                    'desc' => '',
                    'std' => ELECTRONURI . '/img/company_color.png',
                    'type' => 'upload',    
                    'condition' => '',
                    'operator' => 'and',
                    'choices' => array()
                ),
                array(
                    'id' => 'nav_display',
                    'label' => __('Navigation display', 'electron'),
                    'desc' => '',
                    'std' => 'on',
                    'type' => 'on-off',
                    'condition' => '',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'nav_style',
                    'label' => 'Navigation style',
                    'type' => 'select',               
                    'choices' => array(
                            array(
                                'value'       => 'standard',
                                'label'       => __( 'Normal menu', 'electron' ),
                                'src'         => ''
                            ),
                            array(
                                'value'       => 'offcanvas',
                                'label'       => __( 'Off canvas menu', 'electron' ),
                                'src'         => ''
                            )
                        ),
                    'condition' => 'nav_display:is(on)',
                    'operator' => 'and'
                ),
                array(
                  'label'       => 'Header right Icon',
                  'id'          => 'onepage_icon',
                  'type'        => 'iconpicker',
                  'std' => 'fa|fa-phone',
                  'condition' => 'nav_style:not(standard),nav_display:is(off)',
                    'operator' => 'or'
                ),
                array(
                    'id' => 'onepage_right_title',
                    'label' => __('Header right title', 'electron'),
                    'desc' => 'Leave blank to avoid this field',
                    'std' => 'Call Us:',
                    'type' => 'text',
                    'condition' => 'nav_style:not(standard),nav_display:is(off)',
                    'operator' => 'or'
                ),
                array(
                    'id' => 'onepage_right_text',
                    'label' => __('Header right text', 'electron'),
                    'desc' => 'Leave blank to avoid this field',
                    'std' => '312.555.1213',
                    'type' => 'text',
                    'condition' => 'nav_style:not(standard),nav_display:is(off)',
                    'operator' => 'or'
                ),
                array(
                    'id' => 'home_link',
                    'label' => __('Home link', 'electron'),
                    'desc' => '',
                    'std' => 'on',
                    'type' => 'on-off',
                    'condition' => 'nav_display:is(on)',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'home_text',
                    'label' => __('Home Text', 'electron'),
                    'desc' => '',
                    'std' => 'Home',
                    'type' => 'text',
                    'condition' => 'home_link:is(on),nav_display:is(on)',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'one_page_wp_nav',
                    'label' => 'Select onepage nav menu',
                    'desc' => '',
                    'std' => '',
                    'type' => 'taxonomy-select',
                    'taxonomy' => 'nav_menu',
                    'operator' => 'and',
                    'condition' => 'nav_display:is(on)'
                ),
                array(
                    'id' => 'content_settings',
                    'label' => 'Section settings',
                    'type' => 'tab',
                    'operator' => 'and'
                ),
                array(
                    'id' => 'pages',
                    'label' => __('Sections', 'electron'),
                    'desc' => electron_get_page_sections_info(),
                    'std' => '',
                    'type' => 'list-item',
                    'class' => 'electron-settings-page',
                    'condition' => '',
                    'operator' => 'and',
                    'settings' => array(
                        array(
                            'id' => 'page_id',
                            'label' => 'Select a page',        
                            'type' => 'page-select',
                        ),
                        array(
                            'id' => 'background_style',
                            'label' => 'Select Section Background Style',
                            'type' => 'select',               
                            'choices' => array(
                                    array(
                                        'value'       => '',
                                        'label'       => __( 'Default', 'electron' ),
                                        'src'         => ''
                                      ),
                                    array(
                                        'value'       => 'BGlight',
                                        'label'       => __( 'Gray background', 'electron' ),
                                        'src'         => ''
                                      ),
                                    array(
                                        'value'       => 'BGsecondary',
                                        'label'       => __( 'Secondary background', 'electron' ),
                                        'src'         => ''
                                      ),
                                      array(
                                        'value'       => 'BGdark',
                                        'label'       => __( 'Dark background', 'electron' ),
                                        'src'         => ''
                                      ),
                                      array(
                                        'value'       => 'highlightBox',
                                        'label'       => __( 'Image background', 'electron' ),
                                        'src'         => ''
                                      ),

                                )
                        ),
                        array(
                            'id' => 'highlight_background',
                            'label' => 'Background image',        
                            'type' => 'upload',  
                            'std' =>  ELECTRONURI.'/img/slide5.jpg' ,
                            'condition' => 'background_style:is(highlightBox)',
                            'operator' => 'and',                
                        ),
                        array(
                            'id' => 'highlight_overlay',
                            'label' => 'Background overlay style',        
                            'type' => 'select',  
                            'std' =>  '' ,
                            'choices' => array(
                                    array(
                                        'value'       => 'dark-overlay',
                                        'label'       => __( 'Dark overlay', 'electron' ),
                                        'src'         => ''
                                      ),
                                    array(
                                        'value'       => 'light-overlay',
                                        'label'       => __( 'Light overlay', 'electron' ),
                                        'src'         => ''
                                      )

                                ),
                                'condition' => 'background_style:is(highlightBox)',
                                'operator' => 'and',               
                        ),
                        array(
                            'id' => 'section_padding',
                            'label' => 'Section Inner Spacing',        
                            'type' => 'select',  
                            'std' =>  'section-no-padding' ,
                            'choices' => array(
                                    array(
                                        'value'       => '',
                                        'label'       => __( 'Default padding', 'electron' ),
                                        'src'         => ''
                                      ),
                                    array(
                                        'value'       => 'section-no-padding',
                                        'label'       => __( 'No padding', 'electron' ),
                                        'src'         => ''
                                      ),
                                    array(
                                        'value'       => 'section-top-padding-only',
                                        'label'       => __( 'Top padding only', 'electron' ),
                                        'src'         => ''
                                      ),
                                    array(
                                        'value'       => 'section-bottom-padding-only',
                                        'label'       => __( 'Bottom padding only', 'electron' ),
                                        'src'         => ''
                                      )


                                ),
                                'condition' => '',
                                'operator' => 'and',               
                        ),
                        array(
                            'id' => 'container_type',
                            'label' => 'Section container type',        
                            'type' => 'select',  
                            'std' =>  '' ,
                            'choices' => array(
                                    array(
                                        'value'       => 'container',
                                        'label'       => __( 'Default', 'electron' ),
                                        'src'         => ''
                                      ),
                                    array(
                                        'value'       => 'container-wide',
                                        'label'       => __( 'Container Wide', 'electron' ),
                                        'src'         => ''
                                      )

                                ),
                                'condition' => '',
                                'operator' => 'and',               
                        ),
                        array(
                            'id' => 'section_connector',
                            'label' => __('Section connector', 'electron'),
                            'desc' => '',
                            'std' => 'off',
                            'type' => 'on-off',
                            'condition' => '',
                            'operator' => 'and'
                        ),
                        array(
                          'label'       => 'Section Connector Icon',
                          'id'          => 'connector_icon',
                          'type'        => 'iconpicker',
                          'std' => 'fa|fa-plus',
                          'condition' => 'section_connector:is(on)',
                            'operator' => 'and'
                        ),
                    )
                ),
                
            )
        );
        ot_register_meta_box($my_meta_box);
    endif; //if( function_exists( 'ot_get_option' ) ):
}