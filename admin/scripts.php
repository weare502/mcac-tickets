<?php
// Register Style
function electron_admin_styles() {
	wp_register_style( 'electron-font-awesome', ELECTRONURI . '/fonts/font-awesome/css/font-awesome.min.css', false, false );
	wp_register_style( 'electron-electron-icon-picker', ELECTRONURI. '/admin/icon-picker/css/icon-picker.css', false, '1.0.0', 'all' );
	wp_register_style( 'electron-admin-style', ELECTRONURI. '/admin/assets/css/style.css', array( 'electron-electron-icon-picker', 'electron-font-awesome' ), '1.0.0', 'all' );
	wp_enqueue_style( 'electron-admin-style' );
	

}

// Hook into the 'admin_enqueue_scripts' action
add_action( 'admin_enqueue_scripts', 'electron_admin_styles' );

function electron_ot_admin_styles_after(){
	wp_dequeue_style( 'tc-admin-jquery-ui');
}
add_action('ot_admin_styles_after', 'electron_ot_admin_styles_after');


// Register Script
function electron_admin_scripts() {	

	wp_enqueue_script( array ( 'wpdialogs' ) );
	wp_register_script( 'electron-electron-icon-picker', ELECTRONURI. '/admin/icon-picker/js/icon-picker.js', array( 'jquery' ), false, true );
	wp_register_script( 'electron-scripts', ELECTRONURI. '/admin/assets/js/scripts.js', array( 'jquery', 'electron-electron-icon-picker' ), false, true );
	// Localize the script with new data
	$icons = electron_icon_sets();
	wp_localize_script( 'electron-scripts', 'iconset', $icons );
	wp_enqueue_script( 'electron-scripts' );
}

// Hook into the 'admin_enqueue_scripts' action
add_action( 'admin_enqueue_scripts', 'electron_admin_scripts' );
?>