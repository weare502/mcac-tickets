<?php     
/* global vc include files */
$vcfiles = array( 
'posts-template',
'facilities-template',
'performers-template',
'testimonial-carousel',
'special-guests',
'home-slider',
'about-us',
'feature-box',
'sponsors-carousel',
'highlights',
'offer',
'pricing',
'event-location',
'newsletter-form',
'social-icons',
'event-info',
'comming-soon'
);


/* require the files */
foreach ( $vcfiles as $file ) {
	include_once( ELECTRONDIR . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "vc-extends". DIRECTORY_SEPARATOR ."{$file}.php" );
}   

if(function_exists('vc_set_as_theme')):
vc_set_as_theme( $disable_updater = false );
$list = array(
    'page',
    'post',
    'facility'
);
vc_set_default_editor_post_types( $list ); 
endif;