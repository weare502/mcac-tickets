<?php
include(ELECTRONDIR.'/admin/mce-button.php');
include(ELECTRONDIR.'/admin/tickera-functions.php');
include(ELECTRONDIR.'/admin/icon-picker/icon-picker.php');
include(ELECTRONDIR.'/admin/electron-widgets.php');
include(ELECTRONDIR.'/admin/scripts.php');
include(ELECTRONDIR.'/admin/onepage-meta-boxes.php');
include(ELECTRONDIR.'/admin/meta-boxes.php');
include(ELECTRONDIR.'/admin/performer-meta-box.php');
include(ELECTRONDIR.'/admin/vc-extends.php');

function electron_offcanvas_default_menu(){
    echo '<nav id="menu" class="nav">
    <ul class="list-unstyled main-menu">
    <li class="text-right"><a href="#" id="nav-close"><i class="fa fa-close"></i></a></li>
    <li><a href="'.admin_url( 'nav-menus.php' ).'"><i class="fa fa-plus"></i> Menu settings</a></li>
    </ul>
    </nav>';
}

function electron_satandard_default_menu(){
    echo '<div id="menu" class="collapse navbar-collapse"><ul class="nav navbar-nav"><li><a href="'.admin_url( 'nav-menus.php' ).'"><i class="fa fa-plus"></i> Menu settings</a></li></ul></div>';
}

function electron_login_logo() { 
    $logo = (function_exists('ot_get_option'))? ot_get_option('admin_logo', ELECTRONURI.'/images/logo.png') : ELECTRONURI.'/images/logo.png';

    echo '<style type="text/css">
        body.login div#login h1 a {
            background-image: url('.esc_url($logo).');
            background-position: bottom center; 
        }
    </style>';
}
add_action( 'login_enqueue_scripts', 'electron_login_logo' );

function electron_display_image_size_names_muploader( $sizes ) {
  
  $new_sizes = array();
  
  $added_sizes = get_intermediate_image_sizes();
  
  // $added_sizes is an indexed array, therefore need to convert it
  // to associative array, using $value for $key and $value
  foreach( $added_sizes as $key => $value) {
    $new_sizes[$value] = $value;
  }
  
  // This preserves the labels in $sizes, and merges the two arrays
  $new_sizes = array_merge( $new_sizes, $sizes );
  
  return $new_sizes;
}
add_filter('image_size_names_choose', 'electron_display_image_size_names_muploader', 11, 1);

/* filter theme option header */
function electron_header_version_text($output){
  return ELECTRONNAME.' <small>vs</small> '.ELECTRONVERSION ;
}
add_filter('ot_header_version_text', 'electron_header_version_text');

add_action('ot_header_list', 'electron_ot_header_list');
function electron_ot_header_list(){
    echo '<li style="float: right; padding: 13px 12px 0;"><a class="button button-primary right" style="color: #fff;" href="http://event-theme.com/themes/electron/docs/" target="_blank">View online documentation</a></li>';
}

function electron_recognized_font_families( $families ) {
    
    $families['lato'] = 'Raleway';
    $families['oswald'] = 'Oswald';
    
    return $families;    
}
add_filter( 'ot_recognized_font_families', 'electron_recognized_font_families' ) ;

function electron_filter_typography_fields( $array, $field_id ) {

   if (($field_id == "primary_font") || ($field_id == "secondary_font"))  {
      $array = array( 'font-family');
   }

   return $array;

}

add_filter( 'ot_recognized_typography_fields', 'electron_filter_typography_fields', 10, 2 );

add_filter('manage_posts_columns', 'electron_columns_head');
add_action('manage_posts_custom_column', 'electron_columns_content', 10, 2);
add_filter('manage_performer_posts_columns', 'electron_columns_head');
add_action('manage_performer_posts_custom_column', 'electron_columns_content', 10, 2);

// ADD NEW COLUMN
function electron_columns_head($defaults) {
    $defaults['featured_image'] = __('Featured Image', 'electron');
    return $defaults;
}

function electron_columns_content($column_name, $post_ID) {
    global $wpdb;
    if ($column_name == 'featured_image') {
        if (has_post_thumbnail($post_ID)) {
            // HAS A FEATURED IMAGE
           echo get_the_post_thumbnail($post_ID, 'thumbnail');
        }
        else {
            echo '<img src="http://placehold.it/60/000/FFF?text=No+image" alt="No image">';
        }
    }
}

function electron_get_slug($id = null) {
   $post_data = get_post($id, ARRAY_A);
   $slug = $post_data['post_name'];
   return $slug;
}

function electron_onepage_sections_array( $page_id = 0 ){
    $sections = array();
    if($page_id == 0) return $sections;
    $pages = get_post_meta( $page_id, 'pages', true );
    if(!empty($pages)):
        foreach ($pages as $key => $value) {                
                $sections[] = $value['page_id'];
        }       
    endif;
    return $sections;
}

class Electron_walker_nav_menu extends Walker_Nav_Menu {  

    // add classes to ul sub-menus
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // depth dependent classes
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-nav',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
            );
        $class_names = implode( ' ', $classes );
      
        // build html
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }

    // add main/sub classes to li's and links
     function start_el(  &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
      
        // depth dependent classes
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
      
        // passed classes
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
      
        // build html
        $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

        $sections = electron_onepage_sections_array(get_the_id());
        if(!in_array($item->object_id, $sections)){
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        }else{
            $attributes .= ! empty( $item->url )        ? ' href="#'   . esc_attr( electron_get_slug($item->object_id) ) .'"' : '';
        }
        
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

        
        //icons
        $icon_args = array();

        $icon_args['icon_name']           = isset($item->icon_name)? $item->icon_name : '';
        $icon_args['icon_set']            = isset($item->icon_set)? $item->icon_set : 'font-awesome';
        $icon_args['icon_transform']      = isset($item->icon_transform)? $item->icon_transform : '';
        $icon_args['icon_size']           = isset($item->icon_size)? $item->icon_size : '';
        $icon_args['icon_align']          = isset($item->icon_align)? $item->icon_align : '';
        $icon_args['icon_custom_classes'] = isset($item->icon_custom_classes)? $item->icon_custom_classes : '';
        $icon_args['icon_color']          = isset($item->icon_color)? $item->icon_color : '';
        $icon_args['icon_position']       = isset($item->icon_position)? $item->icon_position : 'before';

        if( class_exists('Iconize_WP') ):
            // If hover effect selected, add "iconized-hover-trigger" class to link
            $plugin_instance = Iconize_WP::get_instance();
            $hovers = $plugin_instance->get_iconize_dialog_dropdown_options_for( 'hover' );
            $hovers = array_keys( $hovers );
        endif;

        $link_class = '';

        if ( ! empty( $icon_args['icon_transform'] ) && in_array( $icon_args['icon_transform'], $hovers ) ) {

            $link_class = ' class="iconized-hover-trigger"';
        }

        $icon = '';
        if(function_exists('iconize_get_icon')){
            $icon = iconize_get_icon( $icon_args , 'menu_item' );
        }
        $icon = ($icon == '')? '<i class="fa fa-plus"></i> ' : $icon;
        
        

        if ( 'after' === $icon_args['icon_position'] ) {

            $title = apply_filters( 'the_title', $item->title, $item->ID ) . $icon;

        } else {

            $title = $icon . apply_filters( 'the_title', $item->title, $item->ID );
        }

        //icons end
        
      
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            $title,
            $args->link_after,
            $args->after
        );
      
        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}



function electron_get_posts_dropdown( $args = array() ) {
    global $wpdb, $post;

    $dropdown = array();
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post(); 
            $dropdown[get_the_ID()] = get_the_title();
        }
    }
    wp_reset_postdata();

    return $dropdown;
}

function electron_get_terms( $tax = 'category', $key = 'id' ) {
    $terms = array();
    if ( $key === 'id' ) foreach ( (array) get_terms( $tax, array( 'hide_empty' => false ) ) as $term ) $terms[$term->term_id] = $term->name;
        elseif ( $key === 'slug' ) foreach ( (array) get_terms( $tax, array( 'hide_empty' => false ) ) as $term ) $terms[$term->slug] = $term->name;
            return $terms;
}

if ( ! function_exists( 'electron_default_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Twenty Fourteen 1.0
 *
 * @global WP_Query   $wp_query   WordPress Query object.
 * @global WP_Rewrite $wp_rewrite WordPress Rewrite object.
 */
function electron_default_paging_nav() {
    global $wp_query, $wp_rewrite;

    // Don't print empty markup if there's only one page.
    if ( $wp_query->max_num_pages < 2 ) {
        return;
    }

    $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
    $pagenum_link = html_entity_decode( get_pagenum_link() );
    $query_args   = array();
    $url_parts    = explode( '?', $pagenum_link );

    if ( isset( $url_parts[1] ) ) {
        wp_parse_str( $url_parts[1], $query_args );
    }

    $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
    $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

    $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
    $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

    // Set up paginated links.
    $links = paginate_links( array(
        'base'     => $pagenum_link,
        'format'   => $format,
        'total'    => $wp_query->max_num_pages,
        'current'  => $paged,
        'mid_size' => 1,
        'add_args' => array_map( 'urlencode', $query_args ),
        'prev_text' => __( '&larr; Previous', 'electron' ),
        'next_text' => __( 'Next &rarr;', 'electron' ),
    ) );

    if ( $links ) :

    ?>
    <nav class="navigation paging-navigation" role="navigation">
        <h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'electron' ); ?></h1>
        <div class="pagination loop-pagination">
            <?php echo force_balance_tags($links); ?>
        </div><!-- .pagination -->
    </nav><!-- .navigation -->
    <?php
    endif;
}
endif;

if ( ! function_exists( 'electron_default_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 *
 * @since Twenty Fourteen 1.0
 */
function electron_default_post_nav() {
    // Don't print empty markup if there's nowhere to navigate.
    $previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
    $next     = get_adjacent_post( false, '', false );

    if ( ! $next && ! $previous ) {
        return;
    }

    ?>
    <nav class="navigation post-navigation" role="navigation">
        <h1 class="screen-reader-text"><?php _e( 'Post navigation', 'electron' ); ?></h1>
        <div class="nav-links">
            <?php
            if ( is_attachment() ) :
                previous_post_link( '%link', __( '<span class="meta-nav">Published In</span>%title', 'electron' ) );
            else :
                previous_post_link( '%link', __( '<span class="meta-nav">Previous Post</span>%title', 'electron' ) );
                next_post_link( '%link', __( '<span class="meta-nav">Next Post</span>%title', 'electron' ) );
            endif;
            ?>
        </div><!-- .nav-links -->
    </nav><!-- .navigation -->
    <?php
}
endif;

if(!function_exists('electron_favico_icon')):
    function electron_favico_icon(){
        if(!function_exists('wp_site_icon')){
            $fabicon = ot_get_option('fabicon');
            $apple_icon_72 = ot_get_option('apple_icon_72');
            $apple_icon_114 = ot_get_option('apple_icon_114');
            $apple_icon_144 = ot_get_option('apple_icon_144');

            echo '<!-- Favicon and Apple Icons -->
            <link rel="shortcut icon" href="'.esc_url($fabicon).'">
            <link rel="apple-touch-icon" href="'.esc_url($apple_icon_72).'">
            <link rel="apple-touch-icon" sizes="72x72" href="'.esc_url($apple_icon_114).'">
            <link rel="apple-touch-icon" sizes="114x114" href="i'.esc_url($apple_icon_144).'">';
        }
        
    }
endif;