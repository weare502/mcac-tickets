jQuery(document).ready(function($) {
	 	
	  "use strict";

	  if($('#page_template').length > 0){
		$('#page_template').live('change', function(){

			if( $(this).val() == 'templates/one-page.php'  ){
				$('#electron_onepage_meta_box').show();
				$('#electron_page_meta_box').hide();
			}else{
				$('#electron_onepage_meta_box').hide();
				$('#electron_page_meta_box').show();
			}
			if( $(this).val() == 'templates/comming-soon.php'  ){
				$('#electron_onepage_meta_box').hide();
				$('#electron_page_meta_box').hide();
			}			
			

			return false;
		})

		$('#page_template').trigger('change');
	}

	// Init media buttons
		
		$('.electron-upload-button').live('click', function(e) {
			var $button = $(this),
			$val = $(this).parents('.bp-upload-container').find('input:text'),
			file;
			e.preventDefault();
			e.stopPropagation();
			// If the frame already exists, reopen it
			if (typeof(file) !== 'undefined') file.close();
			// Create WP media frame.
			file = wp.media.frames.perch_media_frame_2 = wp.media({
				// Title of media manager frame
				title: 'Upload image',
				button: {
					//Button text
					text: 'Insert URL'
				},
				// Do not allow multiple files, if you want multiple, set true
				multiple: false
			});
			//callback for selected image
			file.on('select', function() {
				var attachment = file.state().get('selection').first().toJSON();
				$val.val(attachment.url).trigger('change');
			});
			// Open modal
			file.open();
		});
	

	
});


