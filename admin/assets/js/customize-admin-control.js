jQuery(document).ready(function($){
  var customizer_conditions = [];
/*
'' : {
        'condition' : '',
        'operator' : 'and'
      },
*/
  //Static front page
  customizer_conditions['static_front_page'] = {      
      'front_page_header' : {
        'condition' : 'show_on_front:is(page)',
        'operator' : 'and'
      },
      
  };

  //background_image
  customizer_conditions['background_image'] = {
      'top_padding' : {
        'condition' : 'layout_mode:is(boxed)',
        'operator'  : 'and'
      },
      'background_type' : {
        'condition' : 'layout_mode:is(boxed)',
        'operator' : 'and'
      },
      'pattern' : {
        'condition' : 'layout_mode:is(boxed),background_type:is(pattern)',
        'operator' : 'and'
      },
      'body_background_image' : {
        'condition' : 'layout_mode:is(boxed),background_type:is(custom_image)',
        'operator' : 'and'
      },
      'body_background_color' : {
        'condition' : 'layout_mode:is(boxed),background_type:is(custom_image)',
        'operator' : 'and'
      },
      'body_background_repeat' : {
        'condition' : 'layout_mode:is(boxed),background_type:is(custom_image)',
        'operator' : 'and'
      },
      'body_background_size' : {
        'condition' : 'layout_mode:is(boxed),background_type:is(custom_image)',
        'operator' : 'and'
      },
      'body_background_attach' : {
        'condition' : 'layout_mode:is(boxed),background_type:is(custom_image)',
        'operator' : 'and'
      },
      'body_background_position' : {
        'condition' : 'layout_mode:is(boxed),background_type:is(custom_image)',
        'operator' : 'and'
      },
  };

  //header
  customizer_conditions['header'] = {
    'header_search_placeholder' : {
        'condition' : 'header_search_icon:is(on)',
        'operator' : 'and'
      },
    'header_background_color_dark' : {
        'condition' : 'header_background_type:is(dark)',
        'operator' : 'and'
      },
      'header_background_color_light' : {
        'condition' : 'header_background_type:is(light)',
        'operator' : 'and'
      },
      'menu_background_color_dark' : {
        'condition' : 'menu_background_type:is(dark)',
        'operator' : 'and'
      },
      'menu_background_color_light' : {
        'condition' : 'menu_background_type:is(light)',
        'operator' : 'and'
      },
  };

  //sidebar
  customizer_conditions['sidebar'] = {
    'blog_layout_sidebar' : {
        'condition' : 'blog_layout:not(full)',
        'operator' : 'and'
      },
    'single_layout_sidebar' : {
        'condition' : 'single_layout:not(full)',
        'operator' : 'and'
      },
  };
  //blog_settings
  customizer_conditions['blog_settings'] = {
    'home_header_type' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'blog_slider_shortcode' : {
        'condition' : 'blog_header:is(on),home_header_type:is(slider)',
        'operator' : 'and'
      },
      'banner_title' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'banner_subtitle' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'blog_bannerbg_image' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'blog_bannerbg_color' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'blog_bannerbg_attach' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'blog_bannerbg_repeat' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'blog_bannerbg_size' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'blog_bannerbg_position' : {
        'condition' : 'blog_header:is(on)',
        'operator' : 'and'
      },
      'large_posts_display' : {
        'condition' : 'home_post_layout:is(grid)',
        'operator' : 'and'
      },
      'grid_posts_column' : {
        'condition' : 'home_post_layout:not(default)',
        'operator' : 'and'
      },
  };
  //post_settings
  customizer_conditions['post_settings'] = {
    'post_banner_title' : {
        'condition' : 'post_header:is(on)',
        'operator' : 'and'
      },
      'post_banner_subtitle' : {
        'condition' : 'post_header:is(on)',
        'operator' : 'and'
      },
      'post_bannerbg_image' : {
        'condition' : 'post_header:is(on)',
        'operator' : 'and'
      },
      'post_bannerbg_color' : {
        'condition' : 'post_header:is(on)',
        'operator' : 'and'
      },
      'post_bannerbg_attach' : {
        'condition' : 'post_header:is(on)',
        'operator' : 'and'
      },
      'post_bannerbg_repeat' : {
        'condition' : 'post_header:is(on)',
        'operator' : 'and'
      },
      'post_bannerbg_size' : {
        'condition' : 'post_header:is(on)',
        'operator' : 'and'
      },
      'post_bannerbg_position' : {
        'condition' : 'post_header:is(on)',
        'operator' : 'and'
      },
      'related_posts_title' : {
        'condition' : 'related_posts:is(on)',
        'operator' : 'and'
      },
      'total_related_posts' : {
        'condition' : 'related_posts:is(on)',
        'operator' : 'and'
      },
      'related_posts_column' : {
        'condition' : 'related_posts:is(on)',
        'operator' : 'and'
      },
  };

  //footer
  customizer_conditions['footer'] = {
      'widget_area_column' : {
        'condition' : 'footer_widgets_display:is(on)',
        'operator' : 'and'
      },
      'footer_widgets_background_type' : {
        'condition' : 'footer_widgets_display:is(on)',
        'operator' : 'and'
      },
      'footer_widgets_background_image' : {
        'condition' : 'footer_widgets_display:is(on)',
        'operator' : 'and'
      },
      'footer_widgets_background_size' : {
        'condition' : 'footer_widgets_display:is(on)',
        'operator' : 'and'
      },
      'footer_widgets_background_repeat' : {
        'condition' : 'footer_widgets_display:is(on)',
        'operator' : 'and'
      },
      'footer_widgets_background_attach' : {
        'condition' : 'footer_widgets_display:is(on)',
        'operator' : 'and'
      },
      'footer_widgets_background_position' : {
        'condition' : 'footer_widgets_display:is(on)',
        'operator' : 'and'
      },
      'footer_widgets_background_color_dark' : {
        'condition' : 'footer_widgets_display:is(on),footer_widgets_background_type:is(dark)',
        'operator' : 'and'
      },
      'footer_widgets_background_color_light' : {
        'condition' : 'footer_widgets_display:is(on),footer_widgets_background_type:is(light)',
        'operator' : 'and'
      },
      'copyright_text' : {
        'condition' : 'copyright_display:is(on)',
        'operator' : 'and'
      },
      'copyright_background_image' : {
        'condition' : 'copyright_display:is(on)',
        'operator' : 'and'
      },
      'copyright_background_size' : {
        'condition' : 'copyright_display:is(on)',
        'operator' : 'and'
      },
      'copyright_background_repeat' : {
        'condition' : 'copyright_display:is(on)',
        'operator' : 'and'
      },
      'copyright_background_attach' : {
        'condition' : 'copyright_display:is(on)',
        'operator' : 'and'
      },
      'copyright_background_position' : {
        'condition' : 'copyright_display:is(on)',
        'operator' : 'and'
      },
      'copyright_background_color' : {
        'condition' : 'copyright_display:is(on)',
        'operator' : 'and'
      },
      'car_display_item' : {
        'condition' : 'footer_instagram_display:is(on)',
        'operator' : 'and'
      },
      'instagram_num' : {
        'condition' : 'footer_instagram_display:is(on)',
        'operator' : 'and'
      },
      'instagram_type' : {
        'condition' : 'footer_instagram_display:is(on)',
        'operator' : 'and'
      },
      'tagged_id' : {
        'condition' : 'footer_instagram_display:is(on),instagram_type:is(tagged)',
        'operator' : 'and'
      },
      'location_id' : {
        'condition' : 'footer_instagram_display:is(on),instagram_type:is(location)',
        'operator' : 'and'
      },
      'user_id' : {
        'condition' : 'footer_instagram_display:is(on),instagram_type:is(user)',
        'operator' : 'and'
      },
      'car_navigation' : {
        'condition' : 'footer_instagram_display:is(on)',
        'operator' : 'and'
      },
      'car_autoplay' : {
        'condition' : 'footer_instagram_display:is(on)',
        'operator' : 'and'
      },
      'car_autoplay_time' : {
        'condition' : 'footer_instagram_display:is(on),car_autoplay:is(yes)',
        'operator' : 'and'
      },
  };

  


  function electron_match_conditions(condition) {
    var match;
    var regex = /(.+?):(is|not|contains|less_than|less_than_or_equal_to|greater_than|greater_than_or_equal_to)\((.*?)\),?/g;
    var conditions = [];

    while( match = regex.exec( condition ) ) {
      conditions.push({
        'check': match[1], 
        'rule':  match[2], 
        'value': match[3] || ''
      });
    }

    return conditions;
  }

  function electron_condition_objects() {
    return 'select, input[type="radio"]:checked, input[type="checkbox"]:checked,input[type="text"], input[type="hidden"], input.ot-numeric-slider-hidden-input';
  }
  function electron_init_conditions() {
    var delay = (function() {
      var timer = 0;
      return function(callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
      };
      
    })();

    $('.customize-control').on( 'change.conditionals, keyup.conditionals', bohopeople_condition_objects(), function(e) {
      if (e.type === 'keyup') {
        // handle keyup event only once every 500ms
        delay(function() {
          bohopeople_parse_condition();
        }, 500);
      } else {
        bohopeople_parse_condition();
      }
     
    });
    bohopeople_parse_condition();
    
  }

  function electron_parse_condition(){
  	$( '.customize-control[data-condition]' ).each(function() {

           var passed;
          var conditions = bohopeople_match_conditions( $( this ).data( 'condition' ) );
          var operator = ( $( this ).data( 'operator' ) || 'and' ).toLowerCase();


          $.each( conditions, function( index, condition ) {

            var target   = $( '#customize-control-' + condition.check );	         
            var targetEl = !! target.length && target.find( bohopeople_condition_objects() ).first();
            
            if ( ! target.length || ( ! targetEl.length && condition.value.toString() != '' ) ) {
            
              return;
            }

            var v1 = targetEl.length ? targetEl.val().toString() : '';
            var v2 = condition.value.toString();
            var result;

            switch ( condition.rule ) {
              case 'less_than':
                result = ( parseInt( v1 ) < parseInt( v2 ) );
                break;
              case 'less_than_or_equal_to':
                result = ( parseInt( v1 ) <= parseInt( v2 ) );
                break;
              case 'greater_than':
                result = ( parseInt( v1 ) > parseInt( v2 ) );
                break;
              case 'greater_than_or_equal_to':
                result = ( parseInt( v1 ) >= parseInt( v2 ) );
                break;
              case 'contains':
                result = ( v1.indexOf(v2) !== -1 ? true : false );
                break; 
              case 'is':
                result = ( v1 == v2 );
                break;
              case 'not':
                result = ( v1 != v2 );
                break;
            }

            if ( 'undefined' == typeof passed ) {
              passed = result;
            }

            switch ( operator ) {
              case 'or':
                passed = ( passed || result );
                break;
              case 'and':
              default:
                passed = ( passed && result );
                break;
            }
            
          });

          if ( passed ) {
            $(this).animate({opacity: 'show' , height: 'show'}, 200);
          } else {
            $(this).animate({opacity: 'hide' , height: 'hide'}, 200);
          }
          
          delete passed;

      });
  }

  $('li.accordion-section h3.accordion-section-title').on('click', function(){
    var sectionid = $(this).closest('li').attr('id');
    var section = sectionid.replace('accordion-section-', '');
    var obj = customizer_conditions[section];
    if(obj !== 'undefined'){
      
      $.each( obj, function( key, value ) {
        var $id = $('#'+sectionid).find($('li#customize-control-'+key));
        $id.attr('data-condition', value.condition);
        $id.attr('data-operator', value.operator);
      });

      bohopeople_init_conditions();
    }
  })

	
	
})
