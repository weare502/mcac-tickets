<?php
/**
 * The VC Functions
 */
function electron_testimonial_carousel_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Testimonial slider', 'electron' ),
        'base' => 'perch_testimonial_slider',
        'category'     => 'Electron',
        'params' => array(
           array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'electron' ),
                'param_name' => 'testimonials_icon',
                'value' => 'fa fa-quote-left',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),
            // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => __( 'Testimonials', 'electron' ),
                'param_name' => 'testimonials',
                'value' => urlencode( json_encode( array(
                  array(
                    'title' => '* EXTRAORDINARY PERFORMANCES *',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tincidunt tellus vel libero efficitur vehicula Duis molestie tincidunt pellentes Proin elit turpis, blandit in libero in',
                    'name' => 'John Doe',
                    'position' => 'Business Executive'
                  ),
                  array(
                    'title' => '* GREAT MUSIC *',
                    'description' => 'Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam at mauris id massa gravida pulvinar vel id ex. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi. Liquam at mauris id massa gravida pulvinar',
                    'name' => 'Richard Simon',
                    'position' => 'CEO at IT Company'
                  ),
                  array(
                    'title' => '* AWESOME ARRANGEMENT *',
                    'description' => 'Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam at mauris id massa gravida pulvinar vel id ex. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi. Liquam at mauris id massa gravida pulvinar',
                    'name' => 'Natasha Romonoff',
                    'position' => 'Agent'
                  ),
                ) ) ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Title',
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => '',
                        'heading' => 'Description',
                        'param_name' => 'description',
                    ), 
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Client Name',
                        'param_name' => 'name',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Client Position',
                        'param_name' => 'position',
                    ),
                   
                )
            ),
        ),
           
    )
);
}
add_action( 'vc_before_init', 'electron_testimonial_carousel_settings_vc');