<?php
/**
 * The VC Functions
 */
function electron_posts_template_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Posts', 'electron' ),
        'base' => 'perch_posts_template',
        'category'     => 'Electron',
        'params' => array(
            array(
                'type' => 'perch_select',
                'value' => array(                    
                        'templates/blog-carousel.php' => 'Carousel', 
                        'templates/blog-box.php' => 'Grid'
                    ),
                'heading' => 'Posts display',
                'param_name' => 'template',
                'admin_label' => true,
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Hide post date?', 'investment' ),
                'param_name' => 'hide_post_date',
                'value' => array( __( 'Yes', 'investment' ) => 'yes' )                  
            ), 
            array(
                'type' => 'perch_select',
                'value' => array('all' => 'All', 'specific' => 'Specific posts', 'category' => 'Category posts'),
                'heading' => 'Post display',
                'param_name' => 'display',
                'admin_label' => true,
            ),
            // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => __( 'Posts', 'electron' ),
                'param_name' => 'posts',
                'value' => '',
                'params' => array(
                    array(
                        'type' => 'perch_select',
                        'value' => electron_get_posts_dropdown(array('post_type' => 'post', 'posts_per_page' => -1)),
                        'heading' => 'Post',
                        'param_name' => 'post',
                        'admin_label' => true,
                    ),
                   
                ),
                'dependency' => array(
                    'element' => 'display',
                    'value' => 'specific'
                )
            ),
            array(
                'type' => 'perch_select',
                'value' => electron_get_terms(),
                'multiple' => 'multiple',
                'heading' => 'Category',
                'param_name' => 'category',
                'dependency' => array(
                    'element' => 'display',
                    'value' => 'category'
                )
            ),
            array(
                'type' => 'number',
                'value' => '10',
                'heading' => 'Show posts',
                'param_name' => 'posts_per_page',
                'min' => -1,
                'max' => 100,
                'step' => 1,
            ),
            array(
                'type' => 'number',
                'value' => '4',
                'heading' => 'Column',
                'param_name' => 'column',
                'min' => 1,
                'max' => 4,
                'step' => 1,
                'dependency' => array(
                    'element' => 'template',
                    'value' => array('templates/blog-carousel.php', 'templates/blog-box.php')
                )
            ),
            array(
                'type' => 'perch_select',
                'value' => array('no' => 'No', 'yes' => 'Yes'),
                'heading' => 'Autoplay',
                'param_name' => 'autoplay',
                'dependency' => array(
                    'element' => 'template',
                    'value' => 'templates/blog-carousel.php'
                )
            ),
            array(
                'type' => 'perch_select',
                'value' => array('yes' => 'Yes', 'no' => 'No', ),
                'heading' => 'Next/Previous Control display',
                'param_name' => 'control',
                'dependency' => array(
                    'element' => 'template',
                    'value' => 'templates/blog-carousel.php'
                )
            )

        ),
           
    )
    );
}
add_action( 'vc_before_init', 'electron_posts_template_settings_vc');