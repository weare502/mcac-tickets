<?php
/**
 * The VC Functions
 */
function electron_feature_box_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Feature box', 'electron' ),
        'base' => 'perch_feature_box',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(   
           array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'electron' ),
                'param_name' => 'feature_icon',
                'value' => 'fa fa-ticket',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),
            array(
                'type' => 'textfield',
                'value' => 'TICKETS',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
            ),
            array(
            'admin_label' => true,
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => '',
            'heading' => '',
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit in,',
            'description' => __( 'Enter your Feature description.', 'electron' )
         ), 
        ),
           
    )
);
}
add_action( 'vc_before_init', 'electron_feature_box_settings_vc');