<?php
/**
 * The VC Functions
 */
function electron_offer_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Offer', 'electron' ),
        'base' => 'perch_offer',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(  
            array(
                'type' => 'textfield',
                'value' => '30',
                'heading' => 'Large color text',
                'param_name' => 'large_color_text',
            ), 
            array(
                'type' => 'textfield',
                'value' => '%',
                'heading' => 'Small color text',
                'param_name' => 'small_color_text',
            ),  
            array(
                'type' => 'textfield',
                'value' => 'OFF',
                'heading' => 'Small black text',
                'param_name' => 'small_text',
            ),
            array(
                'type' => 'textfield',
                'value' => 'Purchase tickets before 20th MARCH',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
            ), 
                     
            array(
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => '',
            'heading' => '',
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => '<p class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in, ultricies fermentum nulla. Donec in urna sem. Nulla facilisi.</p>',
            'description' => __( 'Enter your content.', 'electron' )
         ), 
            array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => 'Button text',
                'param_name' => 'button_text', 
                'value' => 'Purchase Tickets',
            ), 
            array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => 'Button link',
                'param_name' => 'button_link', 
                'value' => '#',
            ), 
            
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'electron' ),
                'param_name' => 'button_icon',
                'value' => 'fa fa-ticket',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),
        
        
            
    )           
));
}
add_action( 'vc_before_init', 'electron_offer_settings_vc');