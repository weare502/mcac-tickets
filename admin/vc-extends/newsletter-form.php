<?php
/**
 * The VC Functions
 */
function electron_newsletter_form_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Newsletter form', 'electron' ),
        'base' => 'perch_newsletter_form',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(          
           array(
                'type' => 'textfield',
                'value' => 'stay in touch for upcoming events',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
            ),   
            array(
            'type' => 'textarea',
            'holder' => 'div',
            'class' => '',
            'heading' => 'Description',
            'param_name' => 'desc',
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Aenean viverra eget felis quis elementum. ',
            ),   
            array(
                'type' => 'textfield',
                'value' => 'Enter Your Email Address Here...',
                'heading' => 'Email field placeholder',
                'param_name' => 'email_placeholder',
            ),                  
            array(
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => '',
            'heading' => '',
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => '',
            'description' => __( 'Enter your content.', 'electron' ),
         ), 
            
    )           
));
}
if(class_exists('NewsletterModule')){
    add_action( 'vc_before_init', 'electron_newsletter_form_settings_vc');
}
