<?php
/**
 * The VC Functions
 */
function electron_pricing_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Pricing', 'electron' ),
        'base' => 'perch_pricing',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(  
            array(
                'type' => 'checkbox',
                'value' => '',
                'heading' => 'Featured',
                'param_name' => 'featured',
                'admin_label' => true,
            ),
            array(
                'param_name'          => 'pricing_type',
                'heading'       => __( 'Pricing type', 'electron' ),
                'description'        => '',
                'value'         => array(                   
                    'custom' => __( 'Custom', 'electron' ),
                    'tickera' => __( 'Tickera Events', 'electron' ),
                    'eventbrite' => __( 'Eventbrite', 'electron' ),
                  ),
                'type'        => 'perch_select', 
                'admin_label' => true,
            ), 
            array(
                'param_name'          => 'tickera_ticket',
                'heading'       => __( 'Tickera ticket', 'electron' ),
                'description'        => '',
                'value'         => (class_exists( 'TC' ))? electron_events_tickets_array() : array('Tickera plugin is not installed!!'),
                'type'        => 'perch_select', 
                'dependency' => array(
                    'element' => 'pricing_type',
                    'value' => 'tickera'
                ),
                'admin_label' => true,
            ), 
            array(
                'type' => 'textfield',
                'value' => '$',
                'heading' => 'Pricing unit',
                'param_name' => 'price_unit',
                'admin_label' => true,
                'dependency' => array(
                    'element' => 'pricing_type',
                    'value' => array('custom', 'eventbrite')
                ),
            ), 
            array(
                'type' => 'textfield',
                'value' => '499',
                'heading' => 'Price',
                'param_name' => 'price',
                'admin_label' => true,
                'dependency' => array(
                    'element' => 'pricing_type',
                    'value' => array('custom', 'eventbrite')
                ),
            ),  
            array(
                'type' => 'textfield',
                'value' => 'Group of 25',
                'heading' => 'Price small description',
                'param_name' => 'small_text',
                'admin_label' => true,
                'dependency' => array(
                    'element' => 'pricing_type',
                    'value' => array('custom', 'eventbrite')
                ),
            ),
                     
            array(
            'type' => 'textarea',
            'holder' => 'div',
            'class' => '',
            'heading' => 'Pricing details',
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tellus, volutpat quis, mollis auctor nisl. ',
            'description' => __( 'Enter your content.', 'electron' ),
            'dependency' => array(
                    'element' => 'pricing_type',
                    'value' => array('custom', 'eventbrite')
                ),
         ), 
            array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => 'Button text',
                'param_name' => 'button_text', 
                'value' => 'Purchase Tickets',
            ),
            array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => 'Button link',
                'param_name' => 'button_link', 
                'value' => '#',
                'dependency' => array(
                    'element' => 'pricing_type',
                    'value' => 'custom'
                ),
            ), 
            array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => 'Eventbrite ID',
                'param_name' => 'eventbriteid', 
                'value' => '18432753863',
                'dependency' => array(
                    'element' => 'pricing_type',
                    'value' => 'eventbrite'
                ),
            ), 
            
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'electron' ),
                'param_name' => 'button_icon',
                'value' => 'fa fa-ticket',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),
        
        
            
    )           
));
}
add_action( 'vc_before_init', 'electron_pricing_settings_vc');