<?php
/**
 * The VC Functions
 */
function electron_about_us_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'About us', 'electron' ),
        'base' => 'perch_about_us',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(          
            
           array(
                'type' => 'image_upload',
                'value' => '//www.dezinethemes.com/envato/music/01/img/band.png',
                'heading' => 'Large image',
                'param_name' => 'image',
            ),
            array(
                'type' => 'textfield',
                'value' => 'About us',
                'heading' => 'Title',
                'param_name' => 'title',
            ), 
                     
            array(
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => '',
            'heading' => '',
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ex consequat eros lacinia maximus eu sit amet est. Maecenas id risus accumsan, molestie lorem eu, facilisis quam. Vestibulum lacus arcu, cursus vitae elit facilisis, eleifend dignissim magna. Suspendisse luctus dignissim dui, tempus elementum magna venenatis eu. Sed nec vestibulum anteed tortor sem hyn luctus dignis tincidunt et porta dunt et porta dunt et porta vitae, accumsan in dolor.</p>',
            'description' => __( 'Enter your content.', 'electron' )
         ), 
            array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => 'Button text',
                'param_name' => 'button_text', 
                'value' => 'Visit Our Website',
            ), 
            array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => 'Button link',
                'param_name' => 'button_link', 
                'value' => '#',
            ), 
            
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'electron' ),
                'param_name' => 'button_icon',
                'value' => 'fa fa-globe',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),
        
        
            
    )           
));
}
add_action( 'vc_before_init', 'electron_about_us_settings_vc');