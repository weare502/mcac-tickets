<?php
/**
 * The VC Functions
 */
function electron_sponsors_carousel_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Sponsors slider', 'electron' ),
        'base' => 'perch_sponsors_slider',
        'category'     => 'Electron',
        'params' => array(
           array(
                'type' => 'textfield',
                'value' => 'Sponsors',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
                'description' => 'Leave blank to avaoid title'
            ),
            // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => __( 'Sponsors', 'electron' ),
                'param_name' => 'sponsors',
                'value' => urlencode( json_encode( array(
                  array(
                    'title' => 'Sponsors 1',
                    'image' => '//www.dezinethemes.com/envato/music/01/img/sponsors/01.jpg',
                  ),
                  array(
                    'title' => 'Sponsors 2',
                    'image' => '//www.dezinethemes.com/envato/music/01/img/sponsors/02.jpg',
                  ),
                  array(
                    'title' => 'Sponsors 3',
                    'image' => '//www.dezinethemes.com/envato/music/01/img/sponsors/03.jpg',
                  ),
                  array(
                    'title' => 'Sponsors 4',
                    'image' => '//www.dezinethemes.com/envato/music/01/img/sponsors/04.jpg',
                  ),
                  array(
                    'title' => 'Sponsors 5',
                    'image' => '//www.dezinethemes.com/envato/music/01/img/sponsors/05.jpg',
                  ),
                  array(
                    'title' => 'Sponsors 6',
                    'image' => '//www.dezinethemes.com/envato/music/01/img/sponsors/06.jpg',
                  ),
                  array(
                    'title' => 'Sponsors 7',
                    'image' => '//www.dezinethemes.com/envato/music/01/img/sponsors/07.jpg',
                  ),
                ) ) ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Title',
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'image_upload',
                        'value' => '',
                        'heading' => 'Large image',
                        'param_name' => 'image',
                    ),
                   
                )
            ),
            array(
                'type' => 'number',
                'value' => '4',
                'heading' => 'Items show',
                'param_name' => 'column',
                'min' => 1,
                'max' => 20,
                'step' => 1,
            ),
            array(
                'type' => 'perch_select',
                'value' => array('no' => 'No', 'yes' => 'Yes'),
                'heading' => 'Autoplay',
                'param_name' => 'autoplay',
            ),
            array(
                'type' => 'perch_select',
                'value' => array('yes' => 'Yes', 'no' => 'No', ),
                'heading' => 'Next/Previous Control display',
                'param_name' => 'control',
            ),
        ),
           
    )
);
}
add_action( 'vc_before_init', 'electron_sponsors_carousel_settings_vc');