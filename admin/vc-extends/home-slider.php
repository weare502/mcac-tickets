<?php

/**

 * The VC Functions

 */

function electron_home_slider_settings_vc() {

    vc_map(

    array(

      'name'       => __( 'Home slider', 'electron' ),

        'base' => 'perch_home_slider',

        'category'     => 'Electron',

        'content_element' => true,

        'params' => array(   
        	array(
					'type' => 'checkbox',
					'heading' => __( 'Disable slide border', 'investment' ),
					'param_name' => 'disable_border',
					'value' => array( __( 'Yes', 'investment' ) => 'yes' )					
				),       

            // params group

            array(

                'type' => 'param_group',

                'value' => '',

                'heading' => __( 'Slides', 'electron' ),

                'param_name' => 'slides',

                'value' => urlencode( json_encode( array(

				array(

					'title' => __( 'FEEL THE ENERGY', 'electron' ),

					'value' => 'BREATHTAKING CROWD',
					'desc' => '',

					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide1.jpg'

				),

				array(

					'title' => __( 'Shake it out', 'electron' ),

					'subtitle' => 'Rock solid sounds',
					'desc' => '',

					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide2.jpg'

				),

				array(

					'label' => __( 'Electronic Madness', 'electron' ),

					'subtitle' => 'Thrilling performances',
					'desc' => '',

					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide3.jpg'

				),

				) ) ),

                'params' => array(

                	array(

		                'type' => 'image_upload',

		                'value' => '',

		                'heading' => 'Background image',

		                'param_name' => 'bg_image',

		            ),

                    array(

                        'type' => 'textfield',

                        'value' => 'Electronic madness',

                        'heading' => 'Slide title',

                        'param_name' => 'title',

                        'admin_label' => true,

                    ),

                    array(

                        'type' => 'textfield',

                        'value' => 'Thrilling performance',

                        'heading' => 'Subtitle',

                        'param_name' => 'subtitle',

                    ), 
                    array(

                        'type' => 'textfield',

                        'value' => '',

                        'heading' => 'Description',

                        'param_name' => 'desc',

                    ), 

                   

                )

            ),

            array(

	            'type' => 'textfield',

	            'holder' => 'div',

	            'class' => '',

	            'group' => 'Slider footer settings',

	            'heading' => 'Button text',

	            'param_name' => 'button_text', 

	            'value' => 'Join the Event',

         	), 

         	array(

	            'type' => 'textfield',

	            'holder' => 'div',

	            'class' => '',

	            'group' => 'Slider footer settings',

	            'heading' => 'Button link',

	            'param_name' => 'button_link', 

	            'value' => '#',

         	),

         	array(

	            'type' => 'textfield',

	            'holder' => 'div',

	            'class' => '',

	            'group' => 'Slider footer settings',

	            'heading' => 'Countdown time',

	            'param_name' => 'countdown_time', 

	            'value' => 'August 7, 2017 22:00:00',

	            'description' => 'example: August 7, 2017 22:00:00'

         	),

         	array(

	            'param_name'          => 'slide_shape',

	            'heading'       => __( 'Slider Shape', 'bohopeople' ),

	            'description'        => '',

	            'value'         => array(	                

	                'yes' => __( 'Yes', 'bohopeople' ),

	                'no' => __( 'No', 'bohopeople' ),

	              ),

	            'type'        => 'perch_select', 

	         ),

         	array(

	            'param_name'          => 'down_arrow',

	            'heading'       => __( 'Down arrow', 'bohopeople' ),

	            'description'        => '',

	            'value'         => array(	                

	                'yes' => __( 'Yes', 'bohopeople' ),

	                'no' => __( 'No', 'bohopeople' ),

	              ),

	            'type'        => 'perch_select', 

	          ),

        ),

           

    )

);

}

add_action( 'vc_before_init', 'electron_home_slider_settings_vc');