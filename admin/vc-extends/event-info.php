<?php
/**
 * The VC Functions
 */
function electron_event_info_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Event info', 'electron' ),
        'base' => 'perch_event_info',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array( 
            array(
                'param_name'          => 'event_info_type',
                'heading'       => __( 'Event info type', 'electron' ),
                'description'        => '',
                'value'         => array(                   
                    'custom' => __( 'Custom', 'electron' ),
                    'tickera' => __( 'Tickera Events', 'electron' ),
                  ),
                'type'        => 'perch_select', 
                'admin_label' => true,
            ),
            array(
                'param_name'          => 'tickera_event',
                'heading'       => __( 'Tickera event', 'electron' ),
                'description'        => '',
                'value'         => (class_exists( 'TC' ))? electron_events_array() : array('Tickera plugin is not installed!!'),
                'type'        => 'perch_select', 
                'dependency' => array(
                    'element' => 'event_info_type',
                    'value' => 'tickera'
                ),
                'admin_label' => true,
            ), 
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'electron' ),
                'param_name' => 'date_icon',
                'value' => 'fa fa-calendar',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),        
            array(
                'type' => 'textfield',
                'value' => 'Event Date & Time',
                'heading' => 'Title',
                'param_name' => 'date_title',
                'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'value' => '7 August 2017',
                'heading' => 'Date',
                'param_name' => 'date',
                'dependency' => array(
                    'element' => 'event_info_type',
                    'value' => 'custom'
                )
            ),
            array(
                'type' => 'textfield',
                'value' => '10.00 pm',
                'heading' => 'Time',
                'param_name' => 'time',
                'dependency' => array(
                    'element' => 'event_info_type',
                    'value' => 'custom'
                )
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Location icon', 'electron' ),
                'param_name' => 'location_icon',
                'value' => 'fa fa-map-marker',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),        
            array(
                'type' => 'textfield',
                'value' => 'Event Location',
                'heading' => 'Location Title',
                'param_name' => 'location_title',
                'admin_label' => true,
            ),            
            array(
                'type' => 'textarea',
                'value' => '123 Main Street<br>San Francisco, CA 94105, USA',
                'heading' => 'Address',
                'param_name' => 'location',
                'dependency' => array(
                    'element' => 'event_info_type',
                    'value' => 'custom'
                )
            ),            
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Button icon', 'electron' ),
                'param_name' => 'button_icon',
                'value' => 'fa fa-arrow-circle-o-right',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),  
            array(
                'type' => 'textfield',
                'value' => 'Get Directions',
                'heading' => 'Button text',
                'param_name' => 'button_text',
                'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'value' => '#',
                'heading' => 'Button Link',
                'param_name' => 'button_link',
            ),
            
        ),
           
    )
);
}
add_action( 'vc_before_init', 'electron_event_info_settings_vc');