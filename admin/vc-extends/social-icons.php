<?php
/**
 * The VC Functions
 */
function electron_social_icons_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Social icons', 'electron' ),
        'base' => 'perch_social_icons',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(          
            array(
                'type' => 'textfield',
                'value' => 'Questions?',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
            ),
            // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => __( 'Social Links', 'electron' ),
                'param_name' => 'social_links',
                'value' => urlencode( json_encode( array(
                array(
                    'title' => __( 'Follow Us on Facebook', 'electron' ),
                    'link' => '#',
                    'icon' => 'fa fa-facebook'
                ),
                array(
                    'title' => __( 'Follow Us on Twitter', 'electron' ),
                    'link' => '#',
                    'icon' => 'fa fa-twitter'
                ),
                array(
                    'title' => __( 'Watch Our Videos', 'electron' ),
                    'link' => '#',
                    'icon' => 'fa fa-youtube-play'
                ),
                array(
                    'title' => __( 'Follow Us On Pinterest', 'electron' ),
                    'link' => '#',
                    'icon' => 'fa fa-pinterest-square'
                ),
                array(
                    'title' => __( 'Follow Us on LinkedIn', 'electron' ),
                    'link' => '#',
                    'icon' => 'fa fa-linkedin'
                ),
                array(
                    'title' => __( 'Watch Our gallery', 'electron' ),
                    'link' => '#',
                    'icon' => 'fa fa-flickr'
                ),
                array(
                    'title' => __( 'Follow Us on Google+', 'electron' ),
                    'link' => '#',
                    'icon' => 'fa fa-google-plus'
                ),
                array(
                    'title' => __( 'Follow Us on Instagram', 'electron' ),
                    'link' => '#',
                    'icon' => 'fa fa-instagram'
                ),
                ) ) ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Title',
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => '',
                        'heading' => 'Link',
                        'param_name' => 'link',
                    ), 
                    array(
                        'type' => 'iconpicker',
                        'heading' => __( 'Icon', 'electron' ),
                        'param_name' => 'icon',
                        'value' => 'fa fa-heart',
                        'settings' => array(
                            'emptyIcon' => false,
                            // default true, display an "EMPTY" icon?
                            'iconsPerPage' => 4000,
                            // default 100, how many icons per/page to display
                        ),
                        'description' => __( 'Select icon from library.', 'electron' ),
                    )                   
                )              

            ),
            
            
        ),
           
    )
);
}
add_action( 'vc_before_init', 'electron_social_icons_settings_vc');