<?php
/**
 * The VC Functions
 */
function electron_event_location_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Event location', 'electron' ),
        'base' => 'perch_event_location',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(          
            array(
                'type' => 'textfield',
                'value' => 'Event Location',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'value' => '37.791649',
                'heading' => 'Latitude',
                'param_name' => 'latitude',
                'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'value' => '-122.394395',
                'heading' => 'Longitude',
                'param_name' => 'longitude',
                'admin_label' => true,
            ),
            // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => __( 'Address', 'electron' ),
                'param_name' => 'address',
                'value' => urlencode( json_encode( array(
                array(
                    'title' => __( 'Event Location', 'electron' ),
                    'desc' => '123 Main Street<br>San Francisco, CA 94105, USA ',
                    'icon' => 'fa fa-map-marker'
                ),
                array(
                    'title' => __( 'Call Us', 'electron' ),
                    'desc' => '310.55.1213',
                    'icon' => 'fa fa-phone'
                ),
                array(
                    'title' => __( 'Email Us', 'electron' ),
                    'desc' => 'info@yourdomain.com',
                    'icon' => 'fa fa-envelope'
                ),
                ) ) ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Title',
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'value' => '',
                        'heading' => 'Adress description',
                        'param_name' => 'desc',
                    ), 
                    array(
                        'type' => 'iconpicker',
                        'heading' => __( 'Icon', 'electron' ),
                        'param_name' => 'icon',
                        'value' => 'fa fa-star',
                        'settings' => array(
                            'emptyIcon' => false,
                            // default true, display an "EMPTY" icon?
                            'iconsPerPage' => 4000,
                            // default 100, how many icons per/page to display
                        ),
                        'description' => __( 'Select icon from library.', 'electron' ),
                    )                   
                )              

            ),
            array(
                'type' => 'textfield',
                'value' => 'Get Directions',
                'heading' => 'Title',
                'param_name' => 'direction_title',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Destination From:',
                'heading' => 'Destination From text',
                'param_name' => 'direction_from',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => '50 California Street',
                'heading' => 'Destination From Input placeholder',
                'param_name' => 'direction_from_placeholder',
                'admin_label' => false,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Via(Optional):',
                'heading' => 'Destination via text',
                'param_name' => 'direction_via',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Travel mode:',
                'heading' => 'Travel mode title text',
                'param_name' => 'travel_mode_title',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Driving',
                'heading' => 'Travel mode driving text',
                'param_name' => 'travel_mode_driving',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Bicylcing',
                'heading' => 'Travel mode Bicylcing text',
                'param_name' => 'travel_mode_bicylcing',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Public transport',
                'heading' => 'Travel mode transport text',
                'param_name' => 'travel_mode_transport',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Walking',
                'heading' => 'Travel mode Walking text',
                'param_name' => 'travel_mode_walking',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Get Directions',
                'heading' => 'Button text',
                'param_name' => 'button_text',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
                'type' => 'textfield',
                'value' => 'Directions',
                'heading' => 'Direction result title',
                'param_name' => 'direction_result_title',
                'admin_label' => true,
                'group' => 'Direction form'
            ),
            array(
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => '',
            'heading' => '',
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => '<p>Enter <strong>Destination From</strong> under <strong>Get Directions</strong> and Click on Get Directions Button.</p>',
            'description' => __( 'Enter your content.', 'electron' ),
            'group' => 'Direction form'
         ), 
            
        ),
           
    )
);
}
add_action( 'vc_before_init', 'electron_event_location_settings_vc');