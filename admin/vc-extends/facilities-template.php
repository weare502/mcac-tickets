<?php
/**
 * The VC Functions
 */
function electron_facilities_template_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Facilities', 'electron' ),
        'base' => 'perch_facilities',
        'category'     => 'Electron',
        'params' => array(
            array(
                'type' => 'perch_select',
                'value' => array(                    
                        'templates/event-facilities-tabs.php' => 'Tabs', 
                        'templates/event-facilities-carousel.php' => 'Carousel', 
                        'templates/event-facilities-box.php' => 'Grid'
                    ),
                'heading' => 'Facilities display',
                'param_name' => 'template',
            ),
            array(
                'type' => 'perch_select',
                'value' => array('all' => 'All', 'specific' => 'Specific facility'),
                'heading' => 'Performer display',
                'param_name' => 'display',
            ),
            // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => __( 'Facilities', 'electron' ),
                'param_name' => 'facilities',
                'value' => '',
                'params' => array(
                    array(
                        'type' => 'perch_select',
                        'value' => electron_get_posts_dropdown(array('post_type' => 'facility', 'posts_per_page' => -1)),
                        'heading' => 'Facility',
                        'param_name' => 'facility',
                        'admin_label' => true,
                    ),
                   
                ),
                'dependency' => array(
                    'element' => 'display',
                    'value' => 'specific'
                )
            ),
            array(
                'type' => 'number',
                'value' => '4',
                'heading' => 'Column',
                'param_name' => 'column',
                'min' => 1,
                'max' => 4,
                'step' => 1,
                'dependency' => array(
                    'element' => 'template',
                    'value' => array('templates/event-facilities-carousel.php', 'templates/event-facilities-box.php')
                )
            ),
            array(
                'type' => 'perch_select',
                'value' => array('no' => 'No', 'yes' => 'Yes'),
                'heading' => 'Autoplay',
                'param_name' => 'autoplay',
                'dependency' => array(
                    'element' => 'template',
                    'value' => 'templates/event-facilities-carousel.php'
                )
            ),
            array(
                'type' => 'perch_select',
                'value' => array('yes' => 'Yes', 'no' => 'No', ),
                'heading' => 'Next/Previous Control display',
                'param_name' => 'control',
                'dependency' => array(
                    'element' => 'template',
                    'value' => 'templates/event-facilities-carousel.php'
                )
            )

        ),
           
    )
    );
}
add_action( 'vc_before_init', 'electron_facilities_template_settings_vc');