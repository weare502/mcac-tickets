<?php
/**
 * The VC Functions
 */
function electron_highlights_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Highlights', 'electron' ),
        'base' => 'perch_highlights',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(          
           array(
                'type' => 'textfield',
                'value' => 'Highlights',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
            ),                      
            array(
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => '',
            'heading' => '',
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => '<p class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in, ultricies fermentum nulla. Donec in urna sem. Nulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>',
            'description' => __( 'Enter your content.', 'electron' ),
         ), 
            array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => 'Bottom title',
                'param_name' => 'bottom_title', 
                'value' => 'Check it out',
                'admin_label' => true,
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'electron' ),
                'param_name' => 'highlight_icon',
                'value' => 'fa fa-chevron-down',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),
        
        
            
    )           
));
}
add_action( 'vc_before_init', 'electron_highlights_settings_vc');