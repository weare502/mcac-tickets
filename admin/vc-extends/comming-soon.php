<?php
/**
 * The VC Functions
 */
function electron_comming_soon_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Comming soon', 'electron' ),
        'base' => 'perch_comming_soon',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array( 
        	array(
                'type' => 'image_upload',
                'value' => get_template_directory_uri().'/img/slide1.jpg',
                'heading' => 'Background image',
                'param_name' => 'bg_image',
            ),  
        	array(
                'type' => 'textfield',
                'value' => 'coming soon ! ',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
            ),
            array(
                'type' => 'textarea',
                'value' => 'Please check back again within Some Days as We\'re Pretty Close ',
                'heading' => 'Description',
                'param_name' => 'desc',
                'admin_label' => true,
            ),        	 
         	array(
	            'type' => 'textfield',
	            'holder' => 'div',
	            'class' => '',
	            'heading' => 'Countdown time',
	            'param_name' => 'countdown_time', 
	            'value' => 'August 7, 2017 22:00:00',
	            'description' => 'example: August 7, 2017 22:00:00'
         	),
         	array(
	            'param_name'          => 'slide_shape',
	            'heading'       => __( 'Bottom Shape', 'bohopeople' ),
	            'description'        => '',
	            'value'         => array(	                
	                'yes' => __( 'Yes', 'bohopeople' ),
	                'no' => __( 'No', 'bohopeople' ),
	              ),
	            'type'        => 'perch_select', 
	         ),
        ),
           
    )
);
}
add_action( 'vc_before_init', 'electron_comming_soon_settings_vc');