<?php
/**
 * The VC Functions
 */
function electron_special_guests_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Special Guests', 'electron' ),
        'base' => 'perch_special_guests',
        'category'     => 'Electron',
        'content_element' => true,
        'params' => array(
           array(
                'type' => 'image_upload',
                'value' => '//www.dezinethemes.com/envato/music/01/img/special_guest.jpg',
                'heading' => 'Large image',
                'param_name' => 'image',
            ),
           array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'electron' ),
                'param_name' => 'connector_icon',
                'value' => 'fa fa-star',
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'description' => __( 'Select icon from library.', 'electron' ),
            ),
            array(
                'type' => 'textfield',
                'value' => 'Special Guest Performance',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
            ),
            // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => __( 'Single Guest information', 'electron' ),
                'param_name' => 'guest_info',
                'value' => urlencode( json_encode( array(
                array(
                    'title' => __( 'Jenny Doe', 'electron' ),
                    'subtitle' => 'Grammy Award winner',
                    'guest_icon' => 'fa fa-star'
                ),
                array(
                    'title' => __( 'Jen Doe', 'electron' ),
                    'subtitle' => 'Grammy Award winner',
                    'guest_icon' => 'fa fa-star'
                ),
                ) ) ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => 'Jenny Doe',
                        'heading' => 'Guest name',
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => 'Grammy Award winner',
                        'heading' => 'GUest title',
                        'param_name' => 'subtitle',
                    ), 
                    array(
                        'type' => 'iconpicker',
                        'heading' => __( 'Icon', 'electron' ),
                        'param_name' => 'guest_icon',
                        'value' => 'fa fa-star',
                        'settings' => array(
                            'emptyIcon' => false,
                            // default true, display an "EMPTY" icon?
                            'iconsPerPage' => 4000,
                            // default 100, how many icons per/page to display
                        ),
                        'description' => __( 'Select icon from library.', 'electron' ),
                    ),
                   
                )
            ),
            array(
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => '',
            'heading' => '',
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in, ultricies fermentum nulla. Donec in urna sem. Nulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in urna sem. Nulla facilisi.</p>',
            'description' => __( 'Enter your content.', 'electron' )
         ), 
        ),
           
    )
);
}
add_action( 'vc_before_init', 'electron_special_guests_settings_vc');