<?php

/**
 * The VC Functions
 */
function electron_heading_settings_vc() {
	
	vc_map( array(
        'base'          => 'perch_heading',
        'name'       => __( 'Heading', 'bohopeople' ),
        'description'        => '',
        'category'     => 'Bohopeople',
        'class'       => '',
        'icon'   => '',
        'operator'    => '',
        'params'    => array( 
          array(
            'param_name'          => 'style',
            'heading'       => __( 'Style', 'bohopeople' ),
            'description'        => '',
            'value'         => array(
                'default' => __( 'Style 1', 'bohopeople' ),
                '2' => __( 'Style 2', 'bohopeople' ),
              ),
            'type'        => 'perch_select',
          ),
          array(
            'param_name'          => 'title',
            'heading'       => __( 'Title', 'bohopeople' ),
            'description'        => '',
            'value'         => 'Heading title',
            'type'        => 'textfield',     
          ),
          array(
            'param_name'          => 'size',
            'heading'       => __( 'Heading(h1 to h6)', 'bohopeople' ),
            'description'        => '',
            'value'        => '3',
            'type'        => 'number',
            'min' => '1',
            'max' => '6',
            'step' => '1',
          ),
          array(
            'param_name'          => 'align',
            'heading'       => __( 'Text alignment', 'bohopeople' ),
            'description'        => '',
            'value'         => array(
                'center' => __( 'Center', 'bohopeople' ),
                'left' => __( 'Left', 'bohopeople' ),
                'right' => __( 'Right', 'bohopeople' ),
              ),
            'type'        => 'perch_select', 
          ),
          array(
            'param_name'          => 'margin',
            'heading'       => __( 'Margin Bottom (in pixel)', 'bohopeople' ),
            'description'        => '',
            'value'        => '27',
            'type'        => 'number',
            'min' => '1',
            'max' => '100',
            'step' => '1',
          ),
           array(
            'type' => 'textarea',
            'holder' => 'div',
            'class' => '',
            'heading' => __( 'Heading subtitle', 'bohopeople' ),
            'param_name' => 'content', // Important: Only one textarea_html param per content element allowed and it should have 'content' as a 'param_name'
            'value' => __( 'Lorem ipsum dolor sit amet, consect etur adipisic ing elit', 'bohopeople' ),
            'description' => __( 'Enter your content.', 'bohopeople' )
         ),     
        )
    ));
	
}
add_action( 'vc_before_init', 'bohopeople_heading_settings_vc');