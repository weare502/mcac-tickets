<?php
//include all available options
include 'options/general_options.php';
include 'options/background_options.php';
include 'options/header_options.php';
include 'options/sidebar_options.php';
include 'options/footer_options.php';
include 'options/blog_options.php';
include 'options/typography.php';
include 'options/styling_options.php';
include 'options/custom_css.php';
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'electron_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function electron_theme_options() {
  
  /* OptionTree is not loaded yet */
  if ( ! function_exists( 'ot_settings_id' ) )
    return false;
    
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  //available option functions - return type array()
  $general_options = electron_general_options();
  $background_options = electron_background_options();
  $header_options = electron_header_options();
  $sidebar_options = electron_sidebar_options();
  $footer_options = electron_footer_options();
  $blog_options = electron_blog_options();
  $typography_options = electron_typography_options();
  $styling_options = electron_styling_options();
  $custom_css = electron_custom_css();


  //merge all available options
  $settings = array_merge( 
    $general_options, 
    $header_options,
    $background_options, 
    $sidebar_options, 
    $footer_options,  
    $blog_options, 
    $typography_options, 
    $styling_options, 
    $custom_css );

 

  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 
      array(
        'id'          => 'general_options',
        'title'       => __( 'General options', 'electron' )
      ),
      array(
        'id'          => 'header_options',
        'title'       => __( 'Header options', 'electron' )
      ),
      array(
        'id'          => 'background_options',
        'title'       => __( 'Background Options', 'electron' )
      ),     
      array(
        'id'          => 'footer_options',
        'title'       => __( 'Footer options', 'electron' )
      ),
      array(
        'id'          => 'sidebar_option',
        'title'       => __( 'Sidebar options', 'electron' )
      ),
      array(
        'id'          => 'blog_options',
        'title'       => __( 'Blog options', 'electron' )
      ),
      array(
        'id'          => 'typography',
        'title'       => __( 'Typography options', 'electron' )
      ),
      array(
        'id'          => 'styling_options',
        'title'       => __( 'Color options', 'electron' )
      ),
      array(
        'id'          => 'custom_css',
        'title'       => __( 'Custom css', 'electron' )
      )
    ),
    'settings'        => $settings
  );

  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;

  return $custom_settings[ 'settings' ];
  
}