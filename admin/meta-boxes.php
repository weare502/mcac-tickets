<?php
/**
 * Initialize the meta boxes. 
 */

add_action( 'admin_init', 'electron_page_meta_boxes' );


function electron_page_meta_boxes() {
    global $wpdb, $post;
  if( function_exists( 'ot_get_option' ) ): 
  $page_meta_box = array(
    'id'        => 'electron_page_meta_box',
    'title'     => 'Electron page Settings',
    'desc'      => '',
    'pages'     => array( 'page' ),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(       
      array(
        'id'          => 'page_layout',
        'label'       => __( 'Default layout', 'electron' ),
        'desc'        => '',
        'std'         => 'rs',
        'type'        => 'radio-image',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'full',
            'label'       => __( 'Full width', 'electron' ),
            'src'         => OT_URL . '/assets/images/layout/full-width.png'
          ),
          array(
            'value'       => 'ls',
            'label'       => __( 'Left sidebar', 'electron' ),
            'src'         => OT_URL . '/assets/images/layout/left-sidebar.png'
          ),
          array(
            'value'       => 'rs',
            'label'       => __( 'Right sidebar', 'electron' ),
            'src'         => OT_URL . '/assets/images/layout/right-sidebar.png'
          ),
        )
      ),
      array(
        'id'          => 'sidebar',
        'label'       => 'Select sidebar',
        'desc'        => '',
        'std'         => 'sidebar-1',
        'type'        => 'sidebar-select',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
        'condition'   => 'page_layout:not(full)'
      ),           
          
    )
  );
  
  ot_register_meta_box( $page_meta_box );
  endif;  //if( function_exists( 'ot_get_option' ) ):

}