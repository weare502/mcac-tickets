<?php
if ( class_exists( 'TC' ) ) :

	function electron_events_array(){
		global $post;
		$arr = array();
		$wp_events_search = new TC_Events_Search( '', '', -1 );
		foreach ( $wp_events_search->get_results() as $event ) {
			$event = new TC_Event( $event->ID );
			$arr[esc_attr( $event->details->ID )] = $event->details->post_title;			
		}
		if(empty($arr)) $arr[] = esc_attr(__('No events found', 'electron'));

		return $arr;
	}

	function electron_events_tickets_array(){
		$arr = array();
		$wp_tickets_search = new TC_Tickets_Search( '', '', -1 );
		foreach ( $wp_tickets_search->get_results() as $ticket_type ) {
			$ticket = new TC_Ticket( $ticket_type->ID );
			$arr[$ticket->details->ID] = $ticket->details->post_title;			
		}
		if(empty($arr)) $arr[] = esc_attr(__('No Tickets found', 'electron'));

		return $arr;				
	}

	function electron_ticket_cart_button( $atts ) {
		global $tc;

		$tc_general_settings = get_option( 'tc_general_setting', false );

		extract( shortcode_atts( array(
			'featured'				=> false,
			'id'					 => false,
			'title'					 => esc_attr(__( 'Add to Cart', 'electron' )),
			'show_price'			 => false,
			'price_position'		 => 'after',
			'price_wrapper'			 => 'span',
			'price_wrapper_class'	 => 'price',
			'soldout_message'		 => esc_attr(__( 'Tickets are sold out.', 'electron' )),
			'type'					 => 'cart',
			'wrapper'				 => '' ), $atts ) );

		$show_price = (bool) $show_price;

		if ( isset( $id ) ) {
			$ticket_type = new TC_Ticket( $id, 'publish' );
		}

		$event_id = get_post_meta( $id, 'event_name', true );

		if ( isset( $ticket_type->details->ID ) && get_post_status( $event_id ) == 'publish' ) {//check if ticket still exists
			if ( $show_price ) {
				$with_price_content = ' <span class="' . $price_wrapper_class . '">' . do_shortcode( '[ticket_price id="' . $id . '"]' ) . '</span> ';
			} else {
				$with_price_content = '';
			}

			if ( is_array( $tc->get_cart_cookie() ) && array_key_exists( $id, $tc->get_cart_cookie() ) ) {
				$button = sprintf( '<' . $price_wrapper . ' class="tc_in_cart white">%s <a href="%s"><br><span class="title btn '.(($atts['featured'])? 'btn-dark' : 'btn-primary').' btn-sm"><i class="fa fa-shopping-cart"></i> %s</span></a></' . $price_wrapper . '>', __( 'Ticket added to', 'tc' ), $tc->get_cart_slug( true ), __( 'Cart', 'tc' ) );
			} else {
				if ( $ticket_type->is_ticket_exceeded_quantity_limit() === false ) {

					if ( isset( $tc_general_settings[ 'force_login' ] ) && $tc_general_settings[ 'force_login' ] == 'yes' && !is_user_logged_in() ) {
						$button = '<form class="cart_form">'
						. ($price_position == 'before' ? $with_price_content : '') . '<a href="' . wp_login_url( get_permalink() ) . '" class="add_to_cart_force_login" id="ticket_' . $id . '"><span class="title">' . $title . '</span></a>' . ($price_position == 'after' ? $with_price_content : '')
						. '<input type="hidden" name="ticket_id" class="ticket_id" value="' . $id . '"/>'
						. '</form>';
					} else {
						$button = '<form class="cart_form">'
						. ($price_position == 'before' ? $with_price_content : '') . '<a href="#" class="add_to_cart" data-button-type="' . $type . '" id="ticket_' . $id . '"><span class="title btn '.(($atts['featured'])? 'btn-dark' : 'btn-primary').' btn-sm"><i class="'.esc_attr($atts['button_icon']).'"></i> ' . $title . '</span></a>' . ($price_position == 'after' ? $with_price_content : '')
						. '<input type="hidden" name="ticket_id" class="ticket_id" value="' . $id . '"/>'
						. '</form>';
					}
				} else {
					$button = '<span class="tc_tickets_sold">' . $soldout_message . '</span>';
				}
			}

			if ( $id && get_post_type( $id ) == 'tc_tickets' ) {
				return $button;
			} else {
				return esc_attr(__( 'Unknown ticket ID', 'electron' ));
			}
		} else {
			return '';
		}
	}

	add_filter('tc_ticket_added_to_cart_message', 'electron_tc_ticket_added_to_cart_message', 10, 1);
	function electron_tc_ticket_added_to_cart_message($str){
		return '<span class="btn btn-dark btn-sm"><i class="fa fa-shopping-cart"></i>'.$str.'</span>';
	}

	add_filter('tc_ticket_added_to_message', 'electron_tc_ticket_added_to_message', 10, 1);
	function electron_tc_ticket_added_to_message($str){
		return '<span class="white">'.$str.'</span><br>';
	}

	/*class Electron_TC_Shortcodes extends TC_Shortcodes {	

		function tc_additional_fields( $atts ) {
			global $tc;
			ob_start();
			include( ELECTRONDIR . '/templates/tickera/shortcode-cart-additional-info-fields.php' );
			$content = wpautop( ob_get_clean(), true );
			return $content;
		}
		
		function tc_cart_page( $atts ) {
			global $tc;
			ob_start();
			include( ELECTRONDIR . '/templates/tickera/shortcode-cart-contents.php' );
			$content = wpautop( ob_get_clean(), true );
			return $content;
		}


	}

	$electron_shortcodes = new Electron_TC_Shortcodes();*/

endif;