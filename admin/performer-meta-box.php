<?php
/**
 * Initialize the custom Meta Boxes. 
 */
add_action( 'admin_init', 'electron_performer_meta_boxes' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function electron_performer_meta_boxes() {
  
  /**
   * Create a custom meta boxes array that we pass to 
   * the OptionTree Meta Box API Class.
   */
  $performer_meta_box = array(
    'id'          => 'electron_performer_meta_box',
    'title'       => __( 'Performer information', 'electron' ),
    'desc'        => '',
    'pages'       => array( 'performer' ),
    'context'     => 'normal',
    'priority'    => 'low',
    'fields'      => array(
      
      array(
        'label'       => 'Location',
        'id'          => 'location',
        'type'        => 'text',
        'std'        => ' From Los Angeles, CA',
        'desc'        => '',
        'operator'    => 'and',
        'condition'   => ''
      ),
      array(
        'label'       => 'Genre type',
        'id'          => 'genre',
        'type'        => 'text',
        'std'        => ' Gener: Rock!',
        'desc'        => '',
        'operator'    => 'and',
        'condition'   => ''
      ),
      array(
          'id' => 'social_icons_display',
          'label' => __('Social icons display', 'electron'),
          'desc' => '',
          'std' => 'on',
          'type' => 'on-off',
          'condition' => '',
          'operator' => 'and'
      ),      
      array(
        'label'       => __( 'Social icons', 'electron' ),
        'id'          => 'social_icons',
        'type'        => 'list-item',
        'desc'        => '',
        'condition' => 'social_icons_display:is(on)',
        'settings'    => array(
            array(
              'label'       => 'Icon',
              'id'          => 'icon',
              'type'        => 'iconpicker',
            ),
            array(
              'label'       => 'Link',
              'id'          => 'link',
              'type'        => 'text',
            ),
          ),
        'std'         => array(
            array(
              'title' => 'Facebook',
              'icon'  => 'fa|fa-facebook',
              'link'  => '#'
              ),
            array(
              'title' => 'Twitter',
              'icon'  => 'fa|fa-twitter',
              'link'  => '#'
              ),
            array(
              'title' => 'Youtube',
              'icon'  => 'fa|fa-youtube',
              'link'  => '#'
              ),
            array(
              'title' => 'Soundcloud',
              'icon'  => 'fa|fa-soundcloud',
              'link'  => '#'
              ),
          )
      ),
    )
  );

  $facility_meta_box = array(
    'id'          => 'electron_facility_meta_box',
    'title'       => __( 'Facility Icon', 'electron' ),
    'desc'        => '',
    'pages'       => array( 'facility' ),
    'context'     => 'side',
    'priority'    => 'low',
    'fields'      => array(      
      array(
            'id'          => 'icon',
            'type'        => 'iconpicker',
            'std'         => 'fa|fa-map',
          ),
      array(
            'id'          => 'short_desc',
            'type'        => 'textarea',
            'label'       => 'Short Description',
            'desc'        => 'Only display in Facility box. Leave blank to display content in Facility box.',
            'std'         => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit in,',
          ),
    )
  );
  
  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
  if ( function_exists( 'ot_register_meta_box' ) )
    ot_register_meta_box( $performer_meta_box );
    ot_register_meta_box( $facility_meta_box );

}