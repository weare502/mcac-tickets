<?php
function electron_header_options( $options = array() ){
	$options = array(        
      array(
        'id'          => 'logo',
        'label'       => __( 'Logo', 'electron' ),
        'desc'        => 'Appear in Menu bar',
        'std'         => ELECTRONURI. '/img/company_color.png',
        'type'        => 'upload',
        'section'     => 'header_options',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'header_background',
        'label'       => __( 'Header background', 'electron' ),
        'desc'        => '',
        'std'         => array('background-color' => '#4187c9', 'background-image' => ''),
        'type'        => 'background',
        'section'     => 'header_options',        
      ),
		array(
            'id' => 'nav_style',
            'label' => 'Navigation style',
            'type' => 'select', 
            'std' => 'standard',              
            'choices' => array(
                    array(
                        'value'       => 'standard',
                        'label'       => __( 'Normal menu', 'electron' ),
                        'src'         => ''
                    ),
                    array(
                        'value'       => 'offcanvas',
                        'label'       => __( 'Off canvas menu', 'electron' ),
                        'src'         => ''
                    )
                ),
            'condition' => '',
            'operator' => 'and',
            'section'     => 'header_options',   
        ),
        array(
          'label'       => 'Header right Icon',
          'id'          => 'header_icon',
          'type'        => 'iconpicker',
          'std' => 'fa|fa-phone',
          'condition' => 'nav_style:not(standard)',
            'operator' => 'and',
            'section'     => 'header_options',
        ),
        array(
            'id' => 'header_right_title',
            'label' => __('Header right title', 'electron'),
            'desc' => 'Leave blank to avoid this field',
            'std' => 'Call Us:',
            'type' => 'text',
            'condition' => 'nav_style:not(standard)',
            'operator' => 'and',
            'section'     => 'header_options',
        ),
        array(
            'id' => 'header_right_text',
            'label' => __('Header right text', 'electron'),
            'desc' => 'Leave blank to avoid this field',
            'std' => '312.555.1213',
            'type' => 'text',
            'condition' => 'nav_style:not(standard)',
            'operator' => 'and',
            'section'     => 'header_options',
        ),
    
     
    );

	return apply_filters( 'electron_header_options', $options );
}  
?>