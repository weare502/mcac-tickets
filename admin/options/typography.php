<?php
function electron_typography_options( $options = array() ){
	$options = array(      
     array(
        'id'          => 'electron_google_fonts',
        'label'       => __( 'Google Fonts', 'option-tree-theme' ),
        'desc'        => __( 'The Google Fonts option type will dynamically enqueue any number of Google Web Fonts into the document %1$s. As well, once the option has been saved each font family will automatically be inserted 
          into the %2$s array for the Typography option type.', 'electron' ),
        'std'         => array( 
          array(
            'family'    => 'raleway',
            'variants'  => explode(',', '400,100,200,300,500,600,700,800,900'),
            'subsets'   => array( 'latin' )
          ),
          array(
            'family'    => 'oswald',
            'variants'  => array( '300', '400', '700' ),
            'subsets'   => array( 'latin', 'latin-ext' )
          )
        ),
        'type'        => 'google-fonts',
        'section'     => 'typography',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'primary_font',
        'label'       => __( 'Primary font', 'electron' ),
        'desc'        => __( 'It is a global Font setting for whole pages.', 'electron' ),
        'std'         => array(
          'font-family'    => 'raleway',
         ),
        'type'        => 'typography',
        'section'     => 'typography',
        'operator'    => 'and',      
         
      ),
      array(
        'id'          => 'secondary_font',
        'label'       => __( 'Secondary font', 'electron' ),
        'desc'        => __( 'Used this font family in header, heading tag, menu etc', 'electron' ),
        'std'         => array(
          'font-family'    => 'oswald',
         ),
        'type'        => 'typography',
        'section'     => 'typography',
        'operator'    => 'and',      
         
      ),
      array(
        'id'          => 'primary_font',
        'label'       => __( 'Primary font', 'electron' ),
        'desc'        => __( 'It is a global Font setting for whole pages.', 'electron' ),
        'std'         => array(
          'font-family'    => 'raleway',
         ),
        'selector'    => 'body, p', 
        'type'        => 'typography',
        'section'     => 'typography',
        'operator'    => 'and',      
         
      ),
      array(
        'id'          => 'body',
        'label'       => __( 'Body', 'electron' ),
        'desc'        => __( 'It is a global Font setting for whole pages.', 'electron' ),
        'std'         => '',
        'selector'    => 'body, p', 
        'type'        => 'typography',
        'section'     => 'typography',
        'operator'    => 'and',       
         
      ),
      array(
        'id'          => 'h1',
        'label'       => __( 'H1', 'electron' ),
        'desc'        => '',
        'std'         => '',
        'selector'    => 'h1',
        'type'        => 'typography',
        'section'     => 'typography',
        
      ),
      array(
        'id'          => 'h2',
        'label'       => __( 'H2', 'electron' ),
        'desc'        => '',
        'std'         => '',
        'selector'    => 'h2',
        'type'        => 'typography',
        'section'     => 'typography',
        
      ),
      array(
        'id'          => 'h3',
        'label'       => __( 'H3', 'electron' ),
        'desc'        => '',
        'std'         => '',
        'selector'    => 'h3',
        'type'        => 'typography',
        'section'     => 'typography',
      ),
      array(
        'id'          => 'h4',
        'label'       => __( 'H4', 'electron' ),
        'desc'        => '',
        'std'         => '',
        'selector'    => 'h4',
        'type'        => 'typography',
        'section'     => 'typography',
      ),
      array(
        'id'          => 'h5',
        'label'       => __( 'H5', 'electron' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'typography',
      ),
      array(
        'id'          => 'h6',
        'label'       => __( 'H6', 'electron' ),
        'desc'        => '',
        'std'         => '',        
        'type'        => 'typography',
        'section'     => 'typography',
      ),
      array(
        'id'          => 'sidebar_title',
        'label'       => __( 'Sidebar title', 'electron' ),
        'desc'        => '',
        'std'         => '',               
        'type'        => 'typography',
        'section'     => 'typography',
      ),
      array(
        'id'          => 'footer',
        'label'       => __( 'Footer', 'electron' ),
        'desc'        => '',
        'std'         => '',        
        'type'        => 'typography',
        'section'     => 'typography',
      ),
      array(
        'id'          => 'font_css',
        'label'       => __( 'CSS', 'electron' ),
        'class'      => 'hide-field',
        'desc'        => '',
        'std'         => '
html, body, div, p, table, tr, td, th, tbody, tfoot, ul, li, ol, dl, dd, dt, fieldset, blockquote, cite, input, select, textarea, button, a, section, article, aside, header, footer, nav, span,
h1 span, h2 span{
  {{primary_font}}
}        
h1, h1 span, h2, h3, h4, h5, h6, h6 a, .styled div span, .join, .btn, .phone, .offer h1.small span {
    {{secondary_font}}
}        
h1{
{{h1}}
}
h2{
{{h2}}
}
h3{
{{h3}}
}
h4{
{{h4}}
}
h5{
{{h5}}
}
h6{
{{h6}}
}
.sidebar .widget-title{
{{sidebar_title}}
}
#footer{
    {{footer}}
}',
        'type'        => 'css',
        'section'     => 'typography',
        'rows'        => '20',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      
    );

	return apply_filters( 'electron_typography_options', $options );
}