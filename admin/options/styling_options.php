<?php
function electron_styling_options( $options = array() ){
	$options = array(
		array(
        'id'          => 'preset_color_1',
        'label'       => __( 'Preset color 1', 'electron' ),
        'desc'        => '',
        'std'         => '#bd2871',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'preset_color_2',
        'label'       => __( 'Preset color 2', 'electron' ),
        'desc'        => '',
        'std'         => '#ff9000',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'gray_color',
        'label'       => __( 'Global Gray Color', 'electron' ),
        'desc'        => '',
        'std'         => '#f8f8f8',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'dark_color',
        'label'       => __( 'Global Dark Color', 'electron' ),
        'desc'        => '',
        'std'         => '#273034',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'dark_acent_color',
        'label'       => __( 'Global Dark Acent Color', 'electron' ),
        'desc'        => '',
        'std'         => '#232323',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'preset_color_css',
        'label'       => __( 'CSS', 'electron' ),
        'class'      => 'hide-field',
        'desc'        => '',
        'std'         => '


        ',
        'type'        => 'css',
        'section'     => 'styling_options',
      ),
    );

	return apply_filters( 'electron_styling_options', $options );
}  
?>