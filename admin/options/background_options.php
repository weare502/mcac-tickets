<?php
function electron_background_options( $options = array() ){
	$options = array(
		array(
        'id'          => 'container_width',
        'label'       => __( 'Container width', 'electron' ),
        'desc'        => '',
        'std'         => array(1170, 'px'),
        'type'        => 'measurement',
        'section'     => 'background_options',
        'condition'   => '',
        'operator'    => 'and',
      ),
      array(
        'id'          => 'body_background',
        'label'       => __( 'Body background', 'electron' ),
        'desc'        => '',
        'std'         => array('background-color' => '#fff', 'background-image' => ''),
        'type'        => 'background',
        'section'     => 'background_options',        
      ),      
      array(
        'id'          => 'footer_background',
        'label'       => __( 'Footer background', 'electron' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'background_options'        
      ),
      array(
        'id'          => 'background_css',
        'label'       => __( 'CSS', 'electron' ),
        'class'      => 'hide-field',
        'desc'        => '',
        'std'         => '
.container{
    max-width: {{container_width}};
}  
.breadcrumb-bg{
    {{header_background}}
}
body{
    {{body_background}}
}   
footer{
    {{footer_background}}
}   
        ',
        'type'        => 'css',
        'section'     => 'typography',
        'rows'        => '20',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'condition'   => '',
        'operator'    => 'and'
      ),
     
    );

	return apply_filters( 'electron_background_options', $options );
}  
?>