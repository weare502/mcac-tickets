<?php
function electron_general_options( $options = array() ){

    $favicon = array(array(
        'id'          => 'fabicon',
        'label'       => __( 'Fabicon', 'electron' ),
        'desc'        => __( 'Upload or put a url of an ico image that will appear your website\'s (16px X 16px)', 'electron' ),
        'std'         => ELECTRONURI. '/img/icons/favicon.ico',
        'type'        => 'upload',
        'section'     => 'general_options',
        'condition'   => '',
        'operator'    => 'and'
      ),
        array(
        'id'          => 'apple_icon_72',
        'label'       => __( 'Apple Touch Fabicon Icon(72X72)', 'electron' ),
        'desc'        => __( 'Upload or put a url of an ico image that will appear your website\'s', 'electron' ),
        'std'         => ELECTRONURI. '/img/icons/apple-icon-72x72.png',
        'type'        => 'upload',
        'section'     => 'general_options',
        'condition'   => '',
        'operator'    => 'and'
      ),
         array(
        'id'          => 'apple_icon_114',
        'label'       => __( 'Apple Touch Fabicon Icon(114X114)', 'electron' ),
        'desc'        => __( 'Upload or put a url of an ico image that will appear your website\'s', 'electron' ),
        'std'         => ELECTRONURI. '/img/icons/apple-icon-114x114.png',
        'type'        => 'upload',
        'section'     => 'general_options',
        'condition'   => '',
        'operator'    => 'and'
      ),
         array(
        'id'          => 'apple_icon_144',
        'label'       => __( 'Apple Touch Fabicon Icon(144X144)', 'electron' ),
        'desc'        => __( 'Upload or put a url of an ico image that will appear your website\'s', 'electron' ),
        'std'         => ELECTRONURI. '/img/icons/apple-icon-144x144.png',
        'type'        => 'upload',
        'section'     => 'general_options',
        'condition'   => '',
        'operator'    => 'and'
      )
         );

	$options = array(        
		(!function_exists('wp_site_icon'))? $favicon : false,
         array(
        'id'          => 'show_preloader',
        'label'       => __( 'Show Preloader', 'electron' ),
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'general_options',
        'condition'   => '',
        'operator'    => 'and'
      ),
        array(
            'id' => 'preloader',
            'label' => __('Preloader', 'electron'),
            'desc' => '',
            'std' => ELECTRONURI . '/img/Preloader.gif',
            'type' => 'upload',    
            'condition' => 'show_preloader:is(on)',
            'operator' => 'and',
            'choices' => array(),
            'section'     => 'general_options',
        ),
        array(
        'id'          => 'google_map_api',
        'label'       => __( 'Google map API', 'electron' ),
        'desc'        => 'Authentication for the standard API — API keys. <br><a class="button" href="//console.developers.google.com/flows/enableapi?apiid=maps_backend,geocoding_backend,directions_backend,distance_matrix_backend,elevation_backend&keyType=CLIENT_SIDE&reusekey=true" target="_blank"><strong>Get an API key</strong></a><br>
        Example: API for our demo server: <br><u>AIzaSyA27KapuRzpUiNOnrHZRXA7PkKVYdHHOEo</u></small>',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'general_options',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'admin_logo',
        'label'       => __( 'Admin logo', 'electron' ),
        'desc'        => '',
        'std'         => ELECTRONURI. '/img/company_color.png',
        'type'        => 'upload',
        'section'     => 'general_options',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'show_breadcrumbs',
        'label'       => __( 'Show Breadcrumbs', 'electron' ),
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'general_options',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'bredcrumb_menu_prefix',
        'label'       => __( 'Breadcrumbs prefix', 'electron' ),
        'desc'        => '',
        'std'         => 'Home',
        'type'        => 'text',
        'section'     => 'general_options',
        'condition'   => 'show_breadcrumbs:is(on)',
        'operator'    => 'and'
      ),
	);

	return apply_filters( 'electron_general_options', $options );
}
?>