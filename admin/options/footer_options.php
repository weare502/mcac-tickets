<?php
function electron_footer_options( $options = array() ){

    
	$options = array(	
      array(
        'id'          => 'copyright_text',
        'label'       => __( 'Copyright Text', 'landx' ),
        'desc'        => '',
        'std'         => '<p style="text-align:center">Electron &copy; '.date("Y").'. All Rights Reserved.<br> Landing Page Template Designed &amp; Developed By: <a title="Saptarang" href="//themeforest.net/user/saptarang?ref=saptarang"><strong>Saptarang</strong></a></p>',
        'type'        => 'textarea',
        'section'     => 'footer_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
    );

	return apply_filters( 'electron_footer_options', $options );
}  
?>