<?php
function electron_social_options( $options = array() ){
	$options = array(
		array(
        'id'          => 'header_social_icons',
        'label'       => __( 'Header social icons', 'bohopeople' ),
        'desc'        => '',
        'std'         => array(
                array(
                    'title' => 'Facebook',
                    'icon'  => 'fa|fa-facebook',
                    'link'  => '#'
                    ),
                array(
                    'title' => 'Twitter',
                    'icon'  => 'fa|fa-twitter',
                    'link'  => '#'
                    ),
                array(
                    'title' => 'Heart',
                    'icon'  => 'fa|fa-heart',
                    'link'  => '#'
                    ),
                array(
                    'title' => 'Skype',
                    'icon'  => 'fa|fa-skype',
                    'link'  => '#'
                    ),
                array(
                    'title' => 'Linkedin',
                    'icon'  => 'fa|fa-linkedin',
                    'link'  => '#'
                    ),
            ),
        'type'        => 'list-item',
        'section'     => 'social_options',
        'condition'   => '',
        'operator'    => 'and',
        'settings'    => array(           
          array(
            'id'          => 'icon',
            'label'       => __( 'Icons', 'bohopeople' ),
            'desc'        => '',
            'std'         => '',
            'type'        => 'iconpicker',    
            'condition'   => '',
            'operator'    => 'and'
          ),
          array(
            'id'          => 'link',
            'label'       => __( 'Link', 'bohopeople' ),
            'desc'        => '',
            'std'         => '',
            'type'        => 'text',    
            'condition'   => '',
            'operator'    => 'and'
          )
        )
      ),
    );

	return apply_filters( 'bohopeople_theme_text', $options );
}   
?>