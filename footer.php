	<!-- ::: START COPYRIGHT ::: -->
        <footer id="copyright" class="page-block-small BGprime">
            <div class="container text-center"> 
                <div class="row">

                    <?php 
                    $copyright_text = ot_get_option('copyright_text'); 
                    if($copyright_text != ''):
                        echo do_shortcode($copyright_text);
                    else:
                    ?>
                    <p>Electron &copy; <?php echo date("Y") ?>. All Rights Reserved.<br> Landing Page Template Designed &amp; Developed By: <a href="#" title="Saptarang"><strong>jThemes Studio</strong></a></p>
                <?php endif; ?>

                </div><!-- end row -->
            </div><!-- end container -->
        </footer>
        <!-- ::: END MAP ::: -->

        <a href="#slides" class="top"><i class="fa fa-angle-up fa-lg"></i></a>
        <?php get_template_part('templates/eventbrite') ?>

	<?php wp_footer(); ?>
</body>
</html>