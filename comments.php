<?php
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>


<div class="comments-article">
    <div class="clearfix"></div> 
    

	<?php if ( have_comments() ) : ?>
		<div class="article-title" id="comments">
			<h2>
			<?php
				printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'electron' ),
					number_format_i18n( get_comments_number() ) );
			?>
			</h2>
		</div>

		<?php electron_comment_nav(); ?>

		<ol class="comment-list comments-box clearfix">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 80,
					'callback' => 'electron_comments'
				) );
			?>
		</ol><!-- .comment-list -->

		<?php electron_comment_nav(); ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'electron' ); ?></p>
	<?php endif; ?>

	

</div><!-- .comments-area -->
