</div>
<div class="visible-xs space-30"></div>
 <?php
    $layout = electron_get_layout();
    if($layout != 'full'):   
?>
                   
<aside class="sidebar col-sm-4 col-md-3">
    <?php 
        $sidebar = electron_get_sidebar_id();
        if ( is_active_sidebar( $sidebar ) ) : ?>               
                <?php dynamic_sidebar( $sidebar ); ?>               
            <?php 
        else: 
            $args = 'before_widget=<div class="widget-wrap space-bottom-30 categories">&after_widget=</div>&before_title=<h4 class="title-1">&after_title=</h4>'; 
            the_widget( 'WP_Widget_Archives', '', $args ); 
            the_widget( 'WP_Widget_Pages', '', $args ); 
        endif; 
    ?>
</aside> 
<?php endif; ?>