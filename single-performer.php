<?php get_header(); ?>       

    <?php get_template_part( 'templates/content', 'before' ); ?>
    <?php if ( have_posts() ) : ?>

        <?php
        
        // Start the loop.
        while ( have_posts() ) : the_post();  
        	 global $post;
                $location = get_post_meta( $post->ID, 'location', true );
                $genre = get_post_meta( $post->ID, 'genre', true );
                $social_icons_display = get_post_meta( $post->ID, 'social_icons_display', true );
                $social_icons = get_post_meta( $post->ID, 'social_icons', true );
                ?>
                <div id="celebs" class="row">
                <div class="col-md-4">
                    <div class=" item">
                    <div class="img">
                        <?php the_post_thumbnail('electront-performer-size'); ?>
                    </div>
                   
                    <?php if( $location != '' ): ?>
                        <div class="clearfix"><br></div>
                        <i class="fa fa-map-marker"></i> <?php echo esc_attr($location); ?><br>
                    <?php endif; ?>
                    <?php if( $genre != '' ): ?>
                        <i class="fa fa-music"></i> <?php echo esc_attr($genre); ?><br>
                    <?php endif; ?>
                    
                    <?php if( ($social_icons_display != 'off') && !empty($social_icons) ): ?>
                        <ul class="list-inline">
                            <?php foreach ($social_icons as $key => $value) {
                                echo '<li><a href="'.esc_url($value['link']).'" title="'.esc_attr($value['title']).'" target="_blank"><span class="sqaureIconSec">'.electron_ot_get_icon($value['icon']).'</span></a></li>';
                            } ?>
                        </ul>
                    <?php endif; ?>
                    </div>
                </div><!-- end -->
                <div class="col-md-8">
                    <div class="item">
                    <h3 class="text-left"><?php the_title(); ?></h3>
                    <div class="text-left"></div><?php the_content(); ?></div>
                    </div>
                </div>
                </div>
            <?php
        // End the loop.
        endwhile;        
        
        // If no content, include the "No posts found" template.
        else :
        get_template_part( 'templates/content', 'none' );
    endif;
    ?>

    <?php get_sidebar(); ?>

    <?php get_template_part( 'templates/content', 'after' ); ?>   

                   
               


 <?php  get_footer(); ?> 