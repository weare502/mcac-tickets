<?php 
$logo = ot_get_option(  'logo', ELECTRONURI.'/img/company.png' );
$header_icon = ot_get_option( 'header_icon', 'fa|fa-phone' );
$header_right_title = ot_get_option( 'header_right_title', 'Call Us:' );
$header_right_text = ot_get_option( 'header_right_text', '312.555.1213' );
?>
<!-- ::: START HEADER ::: -->
<header id="top" class="off-canvas-menu load-true">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6 logo">
                <a title="<?php bloginfo( 'name' ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img class="img-responsive" src="<?php echo esc_url($logo); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6 rightnav">
                
                <?php if(($header_right_title !='') || ($header_right_text != '')): ?>
                    <ul class="list-inline">
                        <li class="phone text-left">
                            <i class="<?php echo electron_ot_get_icon_class($header_icon); ?> round BGprime"></i>
                            <?php echo ($header_right_title != '')? esc_attr($header_right_title) : '' ?> 
                            <?php echo ($header_right_text != '')? ' <strong>'.esc_attr($header_right_text).'</strong>' : '' ?>
                        </li>
                    </ul>
                <?php endif; ?>
                
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
    
    <?php
        $close = '<li class="text-right"><a href="#" id="nav-close"><i class="fa fa-close"></i></a></li>';
        $args = array(
            'theme_location' => 'primary-menu',
            'container' => 'nav',
            'container_id' => 'menu',
            'container_class' => 'nav',
            'menu_class'     => 'list-unstyled main-menu',                    
            'depth'           => 0,
            'items_wrap'      => '<ul id="%1$s" class="%2$s">'.$close.'%3$s</ul>',
            'walker'          => new Electron_walker_nav_menu(),
            'fallback_cb'  => 'electron_offcanvas_default_menu'
        );

        wp_nav_menu( $args );
        
    ?>

    <div class="navbar navbar-inverse navbar-fixed-top">  
        <div class="navbar-header pull-right">
            <a id="nav-expander" class="nav-expander fixed">
                <i class="fa fa-bars fa-lg white"></i>
            </a>
        </div>
    </div>
    <!-- END NAVIGATION -->

</header>
<!-- ::: END ::: -->