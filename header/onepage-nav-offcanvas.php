<?php
    $onepage_logo = get_post_meta( get_the_ID(), 'onepage_logo', true );
    $onepage_icon = get_post_meta( get_the_ID(), 'onepage_icon', true );
    $onepage_right_title = get_post_meta( get_the_ID(), 'onepage_right_title', true );
    $onepage_right_text = get_post_meta( get_the_ID(), 'onepage_right_text', true );
?>
<!-- ::: START HEADER ::: -->
<header id="top" class="off-canvas-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6 logo">
                <a title="<?php bloginfo( 'name' ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img class="img-responsive" src="<?php echo esc_url($onepage_logo) ?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6 rightnav">
                
                <?php if(($onepage_right_title !='') || ($onepage_right_text != '')): ?>
                    <ul class="list-inline">
                        <li class="phone text-left">
                            <i class="<?php echo electron_ot_get_icon_class($onepage_icon); ?> round BGprime"></i>
                            <?php echo ($onepage_right_title != '')? esc_attr($onepage_right_title) : '' ?> 
                            <?php echo ($onepage_right_text != '')? ' <strong>'.esc_attr($onepage_right_text).'</strong>' : '' ?>
                        </li>
                    </ul>
                <?php endif; ?>
                
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
    
    <?php
        $home_link = get_post_meta( get_the_ID(), 'home_link', true );
        $home_li = '<li class="text-right"><a href="#" id="nav-close"><i class="fa fa-close"></i></a></li>';
        if( $home_link == 'on' ){
            $home_li .= '<li><a href="#home"><i class="fa fa-home"></i>'.get_post_meta( get_the_ID(), 'home_text', true ).'</a></li>';
        }
        $one_page_wp_nav =  get_post_meta( get_the_ID(), 'one_page_wp_nav', true );
        
        $args = array(
            'menu'            => $one_page_wp_nav,
            'container' => 'nav',
            'container_id' => 'menu',
            'container_class' => 'nav',
            'menu_class'     => 'list-unstyled main-menu',                    
            'items_wrap'      => '<ul id="%1$s" class="%2$s">'.$home_li.'%3$s</ul>',
            'depth'           => 0,
            'walker'          => new Electron_walker_nav_menu()
        );

        wp_nav_menu( $args );
        
    ?>

    <div class="navbar navbar-inverse navbar-fixed-top">  
        <div class="navbar-header pull-right">
            <a id="nav-expander" class="nav-expander fixed">
                <i class="fa fa-bars fa-lg white"></i>
            </a>
        </div>
    </div>
    <!-- END NAVIGATION -->

</header>
<!-- ::: END ::: -->