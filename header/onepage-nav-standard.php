<?php
    $onepage_logo = get_post_meta( get_the_ID(), 'onepage_logo', true );
?>
<!-- ::: START HEADER ::: -->
<header id="top" class="standard-menu BGlight">
    <div class="container-fluid">
        <div class="row">
            <div class="logo text-center">
                <a title="<?php bloginfo( 'name' ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                	<img class="img-responsive" src="<?php echo esc_url($onepage_logo); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
            </div>
            <div class="col-sm-12 col-xs-12 rightnav">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <?php
                            $home_link = get_post_meta( get_the_ID(), 'home_link', true );
                            if( $home_link == 'on' ){
                                $home_li = '<li><a href="#home"><i class="fa fa-home"></i>'.get_post_meta( get_the_ID(), 'home_text', true ).'</a></li>';
                            }else{
                                $home_li = '';
                            }
                            $one_page_wp_nav =  get_post_meta( get_the_ID(), 'one_page_wp_nav', true );
                            
                            $args = array(
                                'menu'            => $one_page_wp_nav,
                                'container_id' => 'menu',
                                'container_class' => 'collapse navbar-collapse',
                                'menu_class'     => 'nav navbar-nav',                    
                                'items_wrap'      => '<ul id="%1$s" class="%2$s">'.$home_li.'%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => new Electron_walker_nav_menu()
                            );

                            wp_nav_menu( $args );
                            
                        ?>
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</header>
<!-- ::: END ::: -->