 <!-- CONTENT AREA -->

<!--Breadcrumb Section Start-->
<section class="breadcrumb-bg text-center">          
    <div class="container">
        <div class="site-breadcumb">
            <h1 class="white"><?php echo esc_attr( ot_get_option('blog_title', 'Blog Posts') ); ?></h1>
            <?php echo ( ot_get_option('show_breadcrumbs', 'on') == 'on' )? electron_breadcrumbs() : ''; ?>                    
        </div>  
    </div>
</section>
<!--Breadcrumb Section End-->