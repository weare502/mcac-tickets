<!-- ::: START HEADER ::: -->
<?php 
$logo = ot_get_option(  'logo', ELECTRONURI.'/img/company_color.png' );
?>
<header id="top" class="standard-menu BGlight">
    <div class="container-fluid">
        <div class="row">
            <div class="logo text-center">
                <a title="<?php bloginfo( 'name' ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img class="img-responsive" src="<?php echo esc_url($logo); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
            </div>
            <div class="col-sm-12 col-xs-12 rightnav">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <?php
                            wp_nav_menu( array(
                                'theme_location' => 'primary-menu',
                                'container_id' => 'menu',
                                'container_class' => 'collapse navbar-collapse',
                                'menu_class'     => 'nav navbar-nav',
                                'walker' => new Electron_walker_nav_menu(),
                                'fallback_cb'  => 'electron_satandard_default_menu'
                             ) );
                        ?>
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</header>
<!-- ::: END ::: -->