<?php
    $onepage_logo = get_post_meta( get_the_ID(), 'onepage_logo', true );
    $onepage_icon = get_post_meta( get_the_ID(), 'onepage_icon', true );
    $onepage_right_title = get_post_meta( get_the_ID(), 'onepage_right_title', true );
    $onepage_right_text = get_post_meta( get_the_ID(), 'onepage_right_text', true );
?>
<!-- ::: START HEADER ::: -->
<header id="top" class="off-canvas-menu navigation-off">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6 logo">
                <a title="<?php bloginfo( 'name' ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img class="img-responsive" src="<?php echo esc_url($onepage_logo) ?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6 rightnav">
                
                <?php if(($onepage_right_title !='') || ($onepage_right_text != '')): ?>
                    <ul class="list-inline">
                        <li class="phone text-left">
                            <i class="<?php echo electron_ot_get_icon_class($onepage_icon); ?> round BGprime"></i>
                            <?php echo ($onepage_right_title != '')? esc_attr($onepage_right_title) : '' ?> 
                            <?php echo ($onepage_right_text != '')? ' <strong>'.esc_attr($onepage_right_text).'</strong>' : '' ?>
                        </li>
                    </ul>
                <?php endif; ?>
                
            </div>
        </div><!-- end row -->
    </div><!-- end container -->

</header>
<!-- ::: END ::: -->