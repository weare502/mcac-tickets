<?php
/**
 * Template Name: Comming Soon Template
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php electron_favico_icon(); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php if( ot_get_option('show_preloader', 'on') == 'on' ): ?>
		<div id="preloader">
            <img src="<?php echo esc_url(ot_get_option('preloader', ELECTRONURI.'/img/Preloader.gif')); ?>" alt="Preloader" />
        </div>
    <?php endif; ?>
    <?php 
        if ( have_posts() ) : 
        // Start the loop.
            while ( have_posts() ) : the_post();
                the_content();
            endwhile;
        endif;
        ?>
        
	<?php wp_footer(); ?>
</body>
</html>    