<?php
/**
 * Template Name: One page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in landx consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 */

get_header('onepage'); ?>

<?php
global $wpdb, $post;

$pages = get_post_meta( get_the_ID(), 'pages', true );
if( !empty($pages) ):
	foreach ($pages as $key => $value):			
		if( $value['page_id'] != ''):	
			$pageid = get_the_ID();
			$the_query = new WP_Query( array('p' =>$value['page_id'], 'post_type' => 'page' ) );
			while ( $the_query->have_posts() ) : $the_query->the_post(); 
				$class = array();
				$style = array();
				$slug = electron_get_slug($value['page_id']);
				$class[] = 'section-'.$slug;
				$class[] = $value['background_style'];

				if($value['background_style'] == 'highlightBox'){
					$class[] = 'BGdark';
					$class[] = 'highlight-overlay';
					$class[] = $value['highlight_overlay'];
					$style[] = 'background-image: url('.esc_url($value['highlight_background']).')';
				}
				$class[] = isset($value['section_padding'])? $value['section_padding'] : '';
				$class[] = isset($value['section_connector'])? 'section-connector-'.$value['section_connector'] : 'section-connector-off';				

				$class = array_filter($class);
				$style = array_filter($style);

				$container_type = isset($value['container_type'])? $value['container_type'] : 'container page-block';
				$container_type = ($container_type == 'container')? 'container page-block' : $container_type;
			?>
				<section id="<?php echo esc_attr($slug); ?>" class="<?php echo implode(' ', $class); ?>" style="<?php echo implode('; ', $style); ?>">
					<div class="<?php echo esc_attr($container_type) ?>">
						<?php the_content(); ?>						
					</div>

					<?php if( isset($value['section_connector']) && ($value['section_connector'] == 'on') ): ?>
						<span class="sqaureIconSec absolute more"><?php echo electron_ot_get_icon($value['connector_icon']); ?></span>
					<?php endif; ?>
				</section>
 			<?php 
 			endwhile; // end of the loop. 	
			wp_reset_postdata();			
		endif; //if( $value['page_id'] != ''):
	endforeach;	
endif; //if( !empty($pages) )	
?>

<?php get_footer(); ?>