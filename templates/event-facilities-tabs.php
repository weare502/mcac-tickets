 <?php
        $tab_title = '';
        $tabcontent = '';
        $i = 0;
        // Posts are found
        if ( $posts->have_posts() ) {
            while ( $posts->have_posts() ) :
                $posts->the_post();
                global $post;
                $icon = get_post_meta( $post->ID, 'icon', true );
                $tab_title .= '<li'.(($i==0)? ' class="active"' : '').'><a href="#'.$post->ID.'" data-toggle="tab"><span>'.electron_ot_get_icon($icon).'</span> '.get_the_title($post->ID).'</a></li>';
                $tabcontent .= '<div class="tab-pane'.(($i==0)? ' active' : '').'" id="'.$post->ID.'">
                <h3>'.electron_ot_get_icon($icon).get_the_title($post->ID).'</h3>
                '.apply_filters('the_content', get_the_content($post->ID)).'
                </div><!-- end -->';
                ?>

                <?php
                $i++;
            endwhile;
            ?>
            <div class="tabs clearfix">
                <div class="col-md-4 col-sm-4 col-xs-12"> <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-left">
                        <?php echo force_balance_tags($tab_title); ?>
                    </ul>
                </div>

                <div class="col-md-8 col-sm-8 col-xs-12">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php echo force_balance_tags($tabcontent); ?>
                    </div><!-- end-tab-content -->
                </div>
            </div><!-- end-tabs -->
            <?php
        }
        // Posts not found
        else {
            echo '<h4>' . __( 'Facilities not found', 'perch' ) . '</h4>';
        }

    ?>    
