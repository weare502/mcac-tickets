<div class="owl-carousel" data-autoplay="<?php echo esc_attr($posts->autoplay) ?>" data-column="<?php echo esc_attr($posts->column) ?>"  data-control="<?php echo esc_attr($posts->control) ?>">
        <?php
        // Posts are found
        if ( $posts->have_posts() ) {
            while ( $posts->have_posts() ) :
                $posts->the_post();
                global $post;
                $icon = get_post_meta( $post->ID, 'icon', true );
                $short_desc = get_post_meta( $post->ID, 'short_desc', true );
                $short_desc = ( $short_desc != '' )? $short_desc : wp_trim_words( get_the_excerpt(), 40, '' );
                ?>
                <div class="item">
                    <div class="featureBox text-center">
                        <div class="sqaureIconPrime absolute"><?php echo electron_ot_get_icon($icon) ?></div>
                        <h5><?php the_title(); ?></h5>
                        <p><?php echo esc_attr($short_desc) ?></p>
                    </div>
                </div>
                <?php
            endwhile;
        }
        // Posts not found
        else {
            echo '<h4>' . __( 'Posts not found', 'perch' ) . '</h4>';
        }
    ?>
</div><!-- end Carousel -->