<?php 
$info = $posts->info;
$column = $posts->column; 
?>
<div id="celebs" class="row">
        <?php
        // Posts are found
        if ( $posts->have_posts() ) {
            while ( $posts->have_posts() ) :
                $posts->the_post();
                global $post;
                $location = get_post_meta( $post->ID, 'location', true );
                $genre = get_post_meta( $post->ID, 'genre', true );
                $social_icons = get_post_meta( $post->ID, 'social_icons', true );
                ?>
                <div class="col-md-<?php echo esc_attr(12/$column); ?>">
                <div class="item">
                    <div class="img">
                        <?php the_post_thumbnail('electront-performer-size'); ?>
                    </div>
                    <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>

                     <?php if($info['disable_link'] != 'yes'): ?>
                        <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                    <?php else: ?>
                        <h4><?php the_title(); ?></h4>
                    <?php endif; ?>

                    <?php if( $info['hide_location'] != 'yes' ): ?>
                        <i class="fa fa-map-marker"></i> <?php echo esc_attr($location); ?><br>
                    <?php endif; ?>
                    
                    <?php if( $info['hide_genre'] != 'yes' ): ?>
                        <i class="fa fa-music"></i> <?php echo esc_attr($genre); ?><br>
                    <?php endif; ?>
                    
                    <?php if( $info['hide_social_icons'] != 'yes' ): ?>
                        <ul class="list-inline">
                            <?php foreach ($social_icons as $key => $value) {
                                echo '<li><a href="'.esc_url($value['link']).'" title="'.esc_attr($value['title']).'" target="_blank"><span class="sqaureIconSec">'.electron_ot_get_icon($value['icon']).'</span></a></li>';
                            } ?>
                        </ul>
                    <?php endif; ?>
                </div><!-- end -->
                </div>

                <?php
            endwhile;
        }
        // Posts not found
        else {
            echo '<h4>' . __( 'Posts not found', 'perch' ) . '</h4>';
        }
    ?>
</div><!-- end Carousel -->