<!--Blogs Section Start-->
<div class="container">	
    <div class="row">
        <div class="blog-page-wrap page-block clearfix">
        <?php
			global $wp_query;
			$layout = $wp_query->electron['layout'];
			if($layout != 'full'){		
				$rsclass = ($layout == 'ls')? ' pull-right' : '';
				echo '<div class="col-md-9 col-sm-8 '.$rsclass.'">';
			}else{
				echo '<div class="col-md-12 col-sm-12">';
			}
		?>
