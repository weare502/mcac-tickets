<article id="post-<?php the_ID(); ?>" <?php post_class('post-wrap'); ?>>
    <?php if(has_post_thumbnail()): ?>
	    <div class="post-img">
	        <?php the_post_thumbnail(); ?>
	    </div>
    <?php endif; ?>
    <div class="post-content">
        <?php the_content(); ?>

        <?php
		 	$defaults = array(
				'before'           => '<p><strong>' . __( 'Pages:', 'electron' ).'</strong> ',
				'after'            => '</p>',
				'link_before'      => '',
				'link_after'       => '',
				'next_or_number'   => 'number',
				'separator'        => ' ',
				'nextpagelink'     => __( 'Next page', 'electron' ),
				'previouspagelink' => __( 'Previous page', 'electron' ),
				'pagelink'         => '%',
				'echo'             => 1
			);
 
        	wp_link_pages( $defaults );

		?>
    </div>
</article>