<?php if ( is_search() ) : ?>
    <section>
        <h4><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'electron' ); ?></h4>
        <hr>
        <?php get_search_form(); ?>
        <hr>
    </section>

<?php else : ?>
    <!--  :::  404 ::: -->
    <section class="coming-soon">
        <div class="container text-center">
            <div class="page-block countdown-wrap">                 
                <div class="error-wrap">
                    <h2 class="error-text"> 404 </h2>
                    <hr>                       
                    <p class="big"><i class="fa fa-warning red-color"></i> <?php echo __('Oops! The Page you requested was not found!', 'electron') ?></p>
                    <hr>
                </div>
                <div class="col-md-12 page-block">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn btn-primary smooth"> <?php echo __('Back To Home', 'electron') ?> </a>
                </div>

            </div>
        </div>
        <div class="shapes absShape">

            <div class="shape-1 absShape"></div>
            <div class="shape-2 absShape"></div>              
        </div>
    </section>
    <!-- ::: END ::: -->
<?php endif; ?>