<article id="post-<?php the_ID(); ?>" <?php post_class('post-wrap'); ?>>
    <?php if(has_post_thumbnail()): ?>
    <div class="post-img">
        <?php the_post_thumbnail(); ?>
    </div>
    <?php endif; ?>
    <div class="post-content">
        <?php
            if ( is_sticky() && ! is_paged() ) {
                $sticky_post_text = ot_get_option( 'sticky_post_text', 'Featured post' );
                echo '<span class="sticky-post btn btn-default small">'.sprintf(_x( '%s', 'Sticky post text', 'electron'), esc_attr($sticky_post_text) ).'</span>';
            }

        ?>
        <h6 class="upper-text bold-font"><?php electron_post_categories(); ?></h6>
        <?php if( is_single() ): ?>
            <h6 class="title-1"><?php the_title() ?></h6>
        <?php else: ?>
            <a class="title-1" href="<?php the_permalink(); ?>"><?php the_title() ?></a>
        <?php endif; ?>
        
        <div class="post-meta space-20">
            <span class="post-date">
                <?php echo __('Posted on', 'electron'); ?>
                <a href="<?php echo esc_url( get_permalink() ) ?>"><?php the_time('dS F Y'); ?></a>
            </span>
            <span class="pull-right">
                <?php comments_popup_link( '<i class="fa fa-comment"></i> 0','<i class="fa fa-comment"></i> '.get_comments_number() , '<i class="fa fa-comment"></i> %', 'comments-link', 'Comments are off for this post'); ?></a>
            </span>
        </div>
        <?php 
            if(is_single()){
                the_content();
            }else{
                the_excerpt();
            } 
        ?>
    </div>
    <?php
        $defaults = array(
            'before'           => '<p><strong>' . __( 'Pages:', 'electron' ).'</strong> ',
            'after'            => '</p>',
            'link_before'      => '',
            'link_after'       => '',
            'next_or_number'   => 'number',
            'separator'        => ' ',
            'nextpagelink'     => __( 'Next page', 'electron' ),
            'previouspagelink' => __( 'Previous page', 'electron' ),
            'pagelink'         => '%',
            'echo'             => 1
        );

        wp_link_pages( $defaults );

   
    if ( ('post' == get_post_type()) && is_single() ) {      

        $tags_list = get_the_tag_list( '<strong>Tags</strong>: ', _x( ', ', 'Used between list items, there is a space after the comma.', 'electron' ) );
        if ( $tags_list ) {
            printf( '<span class="tags-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
                _x( 'Tags: ', 'Used before tag names.', 'electron' ),
                $tags_list
            );
        }
    }
    ?>
    <?php if( !is_single() ): ?>
    <div class="post-footer">
        <span class="post-readmore">
            <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm">  <i class="fa fa-caret-right"></i> <?php echo esc_attr( ot_get_option('readmore_text', 'Read More') ); ?> </a>                                    
        </span>
    </div>
    <?php endif; ?>

    <?php edit_post_link( __( 'Edit', 'electron' ), '<p class="entry-footer"><span class="edit-link">', '</span></p><!-- .entry-footer -->' ); ?>
</article>