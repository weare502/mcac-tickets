<?php
$info = $posts->info;
?>
<div class="row" class="hide-postdate-<?php echo esc_attr($info['hide_post_date']) ?>">
    <?php
        // Posts are found
        if ( $posts->have_posts() ) {
            while ( $posts->have_posts() ) :
                $posts->the_post();
                global $post;
                ?>
                <div class="col-md-<?php echo esc_attr(12/$posts->column) ?> col-sm-6 col-xs-12">
                    <div class="img">
                        <?php the_post_thumbnail('electront-blog-size'); ?>
                        <a class="sqaureIconPrime" href="<?php echo get_permalink($post->ID); ?>" title="<?php the_title() ?>"> <i class="fa fa-link"></i> </a>
                    </div>
                    <div class="info">
                        <h6><?php the_title() ?></h6>
                        <span class="post-date"><i class="fa fa-calendar"></i> <?php echo get_the_time('d M, Y', $post->ID); ?></span>
                    </div>
                </div><!-- end -->
            <?php
            endwhile;
        }
        // Posts not found
        else {
            echo '<h4>' . __( 'Posts not found', 'perch' ) . '</h4>';
        }
    ?>
</div><!-- end-carousel -->