<?php
global $wp_query;
if( isset($wp_query->eventbrite) && !empty($wp_query->eventbrite) ):
	$eventbriteArr = $wp_query->eventbrite;
	foreach ($eventbriteArr as $key => $value) : ?>
		<!-- Popup: Register -->
        <div class="modal fade" id="<?php echo esc_attr($key) ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">                 
                <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div  class="popup-register"> 
                    <form id="registration-form" name="registration-form" class="registration-form" action="#" method="post">
                        <iframe  src="//eventbrite.com/tickets-external?eid=<?php echo esc_attr($value) ?>&ref=etckt" frameborder="0" height="300px" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                    </form>                  
                </div>
            </div>
        </div>
        <!-- /Popup: Register -->

	<?php 
	endforeach;
endif; 
?>